package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import travel.agency.business.DTO.RoomDTO;
import travel.agency.business.services.RoomService;

import javax.validation.Valid;

@RestController
@RequestMapping("/rooms/")
public class RoomController {
    @Autowired
    RoomService roomService;

    @PostMapping(path = "addRooms", consumes = "application/json")
    public ResponseEntity addRooms(@Valid @RequestBody RoomDTO roomDTO){
        roomService.addRooms(roomDTO);
        return ResponseEntity.ok().build();
    }

}
