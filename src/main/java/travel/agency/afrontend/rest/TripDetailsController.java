package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.TripDetailsDTO;
import travel.agency.business.DTO.searchDTO.UserAndSearchDetailsDTO;
import travel.agency.business.services.TripDetailsService;

import java.util.List;

@RestController
@RequestMapping("/tripDetails/")
public class TripDetailsController {

    @Autowired
    TripDetailsService tripDetailsService;

    @PostMapping(path = "buyTripDetails", consumes = "application/json")
    public ResponseEntity<String> buyTrip(@RequestBody UserAndSearchDetailsDTO userAndSearchDetailsDTO){
        String message;
        try {
            message = tripDetailsService.buyTrip(userAndSearchDetailsDTO);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(message);
    }

    @GetMapping(path = "getRecentTrips/{userName}")
    public ResponseEntity getRecentTrips(@PathVariable String userName){
        List<TripDetailsDTO> listOfRecentTrips = tripDetailsService.findListOfRecentTrips(userName);
        if(listOfRecentTrips.size() ==0){
            return ResponseEntity.badRequest().body("there are no recent trips for this user");
        }else{
            return ResponseEntity.ok().body(listOfRecentTrips);
        }
    }

}
