package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.CityDTO;
import travel.agency.business.services.CityService;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/cities/")
public class CityController {

    @Autowired
    CityService cityService;

    @PostMapping(path = "addCity", consumes = "application/json")
    public ResponseEntity addCity(@RequestBody CityDTO cityDTO){
        cityService.addCity(cityDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "getListOfCities")
    public List<CityDTO> getListOfCities(){
        List<CityDTO> listOfCities = new LinkedList<>();
        return listOfCities = cityService.findListOfCities();
    }

    @GetMapping(path = "getCitybyName/{name}")
    public CityDTO getCityByName(@PathVariable String name){
        CityDTO cityDTO = cityService.findCityDTOByName(name);
        return cityDTO;
    }


}
