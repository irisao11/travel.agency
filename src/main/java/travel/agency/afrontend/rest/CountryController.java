package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.CountryDTO;
import travel.agency.business.services.CountryService;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/countries/")
public class CountryController {

    @Autowired
    CountryService countryService;

    @PostMapping(path = "addCountry", consumes = "application/json")
    public ResponseEntity addCountry(@RequestBody CountryDTO countryDTO) {
        countryService.addCountry(countryDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "getListOfCountries")
    public List<CountryDTO> getListOfCountries(){
    List<CountryDTO>countryDTOS = new LinkedList<>();
    return countryDTOS = countryService.findListOfCCountries();
    }
}
