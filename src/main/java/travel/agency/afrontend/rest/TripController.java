package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.searchDTO.SearchFromDateToDate;
import travel.agency.business.DTO.searchDTO.SearchTripByDatesDTO;
import travel.agency.business.DTO.searchDTO.SearchTripDetailsDTO;
import travel.agency.business.DTO.TripDTO;
import travel.agency.business.DTO.searchDTO.SearchUserAndTripDTO;
import travel.agency.business.services.TripService;

import java.util.List;

@RestController
@RequestMapping("/trips/")
public class TripController {

    @Autowired
    TripService tripService;

    @PostMapping(path = "addTrip", consumes = "application/json")
    public ResponseEntity addTrip(@RequestBody SearchUserAndTripDTO searchUserAndTripDTO) {

        boolean tripAdded = tripService.addTrip(searchUserAndTripDTO);
        if(tripAdded){
            return ResponseEntity.ok().body("Trip added successfully");
        }else {
            return ResponseEntity.badRequest().body("You are not allowed to make this operation");
        }
    }

    @GetMapping(path = "getSearchedTrip", consumes = "application/json")
    public TripDTO getSearchedTrips(@RequestBody SearchTripDetailsDTO searchTripDetailsDTO) {
        TripDTO chosenTrip = tripService.findTripToCityFromDateToDateAndNumOfPersons(searchTripDetailsDTO);
        return chosenTrip;
    }

    @GetMapping(path = "getTripsByDetails", consumes = "application/json")
    public List<TripDTO> getTripsByDetails(@RequestBody SearchTripByDatesDTO searchTripByDatesDTO) {
        List<TripDTO> listOfTripsByDetails = tripService.findListOfTripsByDetails(searchTripByDatesDTO);
        return listOfTripsByDetails;
    }

    @GetMapping(path = "getTripsByCountries")
    public List<TripDTO> getTripsByCountries() {
        List<TripDTO> sortedTripsByCountries = tripService.sortByCountryDTO();
        return sortedTripsByCountries;
    }

    @GetMapping(path = "getTripsByContinents")
    public List<TripDTO> getTripsByContinents() {
        List<TripDTO> sortedTripsByContinents = tripService.sortByContinentDTO();
        return sortedTripsByContinents;
    }

    @GetMapping(path = "getAllTrips")
    public List<TripDTO> getAllTrips() {
        List<TripDTO> listOfAllTrips = tripService.findAllTrips();
        return listOfAllTrips;
    }

    @GetMapping(path = "getPromotedTrips")
    public List<TripDTO> listOfPromotedTrips() {
        List<TripDTO> listOfPromotedTrips = tripService.sortByPromoted();
        return listOfPromotedTrips;
    }

    @GetMapping(path = "getUpcomingTrips")
    public ResponseEntity getUpcomingTrips() {
        List<TripDTO> upcomingTrips = tripService.getListOfUpcomingTrips();
        if(upcomingTrips.size() == 0){
            return ResponseEntity.badRequest().body("There are no upcoming trips");
        }else{
            return ResponseEntity.ok().body(upcomingTrips);
        }
    }

    @GetMapping(path = "getTripsByCityOfDeparture/{departureCity}")
    public ResponseEntity getTripsFromCity(@PathVariable String departureCity){
        List<TripDTO> listOfTripsFromCity = tripService.getListOfTripsFromCity(departureCity);
        if(listOfTripsFromCity.size() == 0){
            return ResponseEntity.badRequest().body("There are no trips from this city");
        }else{
            return ResponseEntity.ok().body(listOfTripsFromCity);
        }
    }

    @GetMapping(path = "getTripsWithDepartureDateBetweenDates", consumes = "application/json")
    public ResponseEntity getTripsWithDepartureDateBetweenDates(@RequestBody SearchFromDateToDate searchFromDateToDate){
        List<TripDTO> tripsFromDateToDate = tripService.getTripsWithDepartureDateBetweenDates(searchFromDateToDate);
        if(tripsFromDateToDate.size() == 0){
            return ResponseEntity.badRequest().body("There are no trips with departure date between these dates");
        }else{
            return ResponseEntity.ok().body(tripsFromDateToDate);
        }
    }

    @GetMapping(path = "getTripsWithNumberOfNights/{numberOfNights}")
    public ResponseEntity getTripsWithNumberOfNights(@PathVariable long numberOfNights){
        List<TripDTO> tripsWithNumberOfNights = tripService.getTripsOfNumberOfNights(numberOfNights);
        if(tripsWithNumberOfNights.size()==0){
            return ResponseEntity.badRequest().body("There are no trips with that number of nights available");
        }else{
            return ResponseEntity.ok().body(tripsWithNumberOfNights);
        }
    }

}
