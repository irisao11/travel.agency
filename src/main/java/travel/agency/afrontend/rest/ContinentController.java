package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import travel.agency.business.DTO.ContinentDTO;
import travel.agency.business.services.ContinentService;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/continents/")
public class ContinentController {

    @Autowired
    ContinentService continentService;

    @GetMapping(path="getListOfContinents")
    public List<ContinentDTO> getListOfContinents(){
        List<ContinentDTO> continentDTOS = new LinkedList<>();
        return continentDTOS = continentService.findListOfContinents();
    }
}
