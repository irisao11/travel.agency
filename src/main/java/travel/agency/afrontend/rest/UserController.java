package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.UserDTO;
import travel.agency.business.services.UserService;

@RestController
@RequestMapping("/users/")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(path = "addUser", consumes = "application/json")
    public ResponseEntity addUser(@RequestBody UserDTO userDTO) {
        userService.addUser(userDTO);
    return ResponseEntity.ok().build();
    }

    @GetMapping(path = "findUser", consumes = "application/json")
    public UserDTO findUser(@RequestBody UserDTO userDTO){
        return userService.findUserByNameAndPassword(userDTO);
    }
}
