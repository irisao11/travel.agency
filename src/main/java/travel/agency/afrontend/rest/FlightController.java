package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.CityDTO;
import travel.agency.business.DTO.FlightDTO;
import travel.agency.business.DTO.searchDTO.SearchFlightDTO;
import travel.agency.business.services.FlightService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/flights/")
public class FlightController {
    @Autowired
    FlightService flightService;

    @PostMapping(path = "addFlight", consumes = "application/json")
    public ResponseEntity addFlight(@Valid @RequestBody FlightDTO flightDTO){
        flightService.addFlight(flightDTO);
    return ResponseEntity.ok().build();
    }

    @GetMapping(path ="getFlightByNumber/{flightNumber}")
    public FlightDTO getFlightByNumber(@PathVariable String flightNumber){
        FlightDTO flightDTO = flightService.findFlightDTOtByNumber(flightNumber);
    return flightDTO;
    }

    @GetMapping(path = "getFlightsFromCityToCity", consumes = "application/json")
    public List<FlightDTO> getFlightsFromCityToCity(@RequestBody SearchFlightDTO searchFlightDTO){
        List<FlightDTO> listOfFlights = flightService.findFlightsFromCityToCity(searchFlightDTO);
    return listOfFlights;
    }

    @GetMapping(path = "getFlightInCity", consumes = "application/json")
    public List<FlightDTO> getFlightInCity(@RequestBody CityDTO cityDTO){
        List<FlightDTO>flightDTOS = flightService.findFlightFromCity(cityDTO);
    return flightDTOS;
    }

    @GetMapping(path = "getFlightFromCityToCityAndDate", consumes = "application/json")
    public List<FlightDTO> getFlightFromCityToCityAndDate(@RequestBody SearchFlightDTO searchFlightDTO){
        List<FlightDTO> flightDTOS = flightService.findFlightsFromCityToCityAndDate(searchFlightDTO);
    return flightDTOS;
    }


}
