package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.AirportDTO;
import travel.agency.business.DTO.CityDTO;
import travel.agency.business.services.AirportService;

import java.util.List;

@RestController
@RequestMapping("/airports/")
public class AirportController {
    @Autowired
    AirportService airportService;

    @PostMapping(path = "/addAirport", consumes = "application/json")
    public ResponseEntity addAirport(@RequestBody AirportDTO airportDTO){
        airportService.addAirport(airportDTO);
    return ResponseEntity.ok().build();
    }

    @GetMapping(path = "getAirportsByCity", consumes = "application/json")
     public List<AirportDTO> getAirportsByCity(@RequestBody CityDTO cityDTO){
        List<AirportDTO> listOfAirportsByCity = airportService.findAirportsInCity(cityDTO);
     return listOfAirportsByCity;
    }

}
