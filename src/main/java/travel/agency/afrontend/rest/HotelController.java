package travel.agency.afrontend.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travel.agency.business.DTO.HotelDTO;
import travel.agency.business.DTO.RoomDTO;
import travel.agency.business.services.HotelService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/hotels/")
public class HotelController {
    @Autowired
    HotelService hotelService;

    @PostMapping(path="addHotel", consumes = "application/json")
    public ResponseEntity addHotel(@Valid @RequestBody HotelDTO hotelDTO){
        hotelService.addHotel(hotelDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "getListOfRooms", consumes = "application/json")
    public List<RoomDTO> getListOfRooms(@RequestBody HotelDTO hotelDTO){
        return hotelService.findRoomsDTOInHotel(hotelDTO);
    }

    @GetMapping(path = "getHotelByName/{name}/{cityName}")
    public HotelDTO findHotelByName(@PathVariable String name, @PathVariable String cityName){
        HotelDTO hotelDTO = hotelService.findHotelByStringName(name, cityName);
        return hotelDTO;
    }

    @GetMapping(path = "getAllHotels")
    public List<HotelDTO> getAllHotels(){
        List<HotelDTO> listOfAllHotels = hotelService.findListOfAllHotels();
        return listOfAllHotels;
    }

}
