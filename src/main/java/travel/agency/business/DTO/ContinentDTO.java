package travel.agency.business.DTO;

public class ContinentDTO {
    private String name;

    public ContinentDTO() {
    }

    public ContinentDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ContinentDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
