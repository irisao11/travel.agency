package travel.agency.business.DTO;

import travel.agency.persistance.entities.Country;

public class CityDTO {
    private String name;
    private CountryDTO countryDTO;

    public CityDTO() {
    }

    public CityDTO(String name, CountryDTO countryDTO) {
        this.name = name;
        this.countryDTO = countryDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryDTO getCountryDTO() {
        return countryDTO;
    }

    public void setCountryDTO(CountryDTO countryDTO) {
        this.countryDTO = countryDTO;
    }

    @Override
    public String toString() {
        return "CityDTO{" +
                "name='" + name + '\'' +
                ", countryDTO=" + countryDTO +
                '}';
    }
}
