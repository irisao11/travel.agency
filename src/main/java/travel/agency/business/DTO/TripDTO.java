package travel.agency.business.DTO;

import org.springframework.stereotype.Repository;


@Repository
public class TripDTO {
    private FlightDTO departureFlightDTO;
    private FlightDTO returnFlightDTO;
    private HotelDTO hotelDTO;
    private boolean promoted;

    public TripDTO() {
    }

    public TripDTO(FlightDTO departureFlightDTO, FlightDTO returnFlightDTO, HotelDTO hotelDTO, boolean promoted) {
        this.departureFlightDTO = departureFlightDTO;
        this.returnFlightDTO = returnFlightDTO;
        this.hotelDTO = hotelDTO;
        this.promoted = promoted;
    }

    public FlightDTO getDepartureFlightDTO() {
        return departureFlightDTO;
    }

    public void setDepartureFlightDTO(FlightDTO departureFlightDTO) {
        this.departureFlightDTO = departureFlightDTO;
    }

    public FlightDTO getReturnFlightDTO() {
        return returnFlightDTO;
    }

    public void setReturnFlightDTO(FlightDTO returnFlightDTO) {
        this.returnFlightDTO = returnFlightDTO;
    }

    public HotelDTO getHotelDTO() {
        return hotelDTO;
    }

    public void setHotelDTO(HotelDTO hotelDTO) {
        this.hotelDTO = hotelDTO;
    }

    public boolean isPromoted() {
        return promoted;
    }

    public void setPromoted(boolean promoted) {
        this.promoted = promoted;
    }

    @Override
    public String toString() {
        return "TripDTO{" +
                "departureFlightDTO=" + departureFlightDTO +
                ", returnFlightDTO=" + returnFlightDTO +
                ", hotelDTO=" + hotelDTO +
                ", promoted=" + promoted +
                '}';
    }
}
