package travel.agency.business.DTO.searchDTO;

import travel.agency.business.DTO.CityDTO;

import java.sql.Date;

public class SearchTripByDatesDTO {
    private Date departureDate;
    private Date returnDate;
    private CityDTO departureCity;
    private CityDTO returnCity;
    private Long numberOfPersons;

    public SearchTripByDatesDTO() {
    }

    public SearchTripByDatesDTO(Date departureDate, Date returnDate, CityDTO departureCity, CityDTO returnCity, Long numberOfPersons) {
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.departureCity = departureCity;
        this.returnCity = returnCity;
        this.numberOfPersons = numberOfPersons;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public CityDTO getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(CityDTO departureCity) {
        this.departureCity = departureCity;
    }

    public CityDTO getReturnCity() {
        return returnCity;
    }

    public void setReturnCity(CityDTO returnCity) {
        this.returnCity = returnCity;
    }

    public Long getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(Long numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    @Override
    public String toString() {
        return "SearchTripByDatesDTO{" +
                "departureDate=" + departureDate +
                ", returnDate=" + returnDate +
                ", departureCity=" + departureCity +
                ", returnCity=" + returnCity +
                ", numberOfPersons=" + numberOfPersons +
                '}';
    }
}
