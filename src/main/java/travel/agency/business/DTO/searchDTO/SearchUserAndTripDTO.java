package travel.agency.business.DTO.searchDTO;

import travel.agency.business.DTO.TripDTO;
import travel.agency.business.DTO.UserDTO;

public class SearchUserAndTripDTO {
    private UserDTO userDTO;
    private TripDTO tripDTO;

    public SearchUserAndTripDTO() {
    }

    public SearchUserAndTripDTO(UserDTO userDTO, TripDTO tripDTO) {
        this.userDTO = userDTO;
        this.tripDTO = tripDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public TripDTO getTripDTO() {
        return tripDTO;
    }

    public void setTripDTO(TripDTO tripDTO) {
        this.tripDTO = tripDTO;
    }

    @Override
    public String toString() {
        return "SearchUserAndTripDTO{" +
                "userDTO=" + userDTO +
                ", tripDTO=" + tripDTO +
                '}';
    }
}
