package travel.agency.business.DTO.searchDTO;

import travel.agency.business.DTO.CityDTO;
import travel.agency.persistance.entities.City;

import java.sql.Date;

public class SearchFlightDTO {

    private CityDTO departureCity;
    private CityDTO returnCity;
    private Date departureDate;


    public SearchFlightDTO() {
    }

    public SearchFlightDTO(CityDTO departureCity, CityDTO returnCity, Date departureDate) {
        this.departureCity = departureCity;
        this.returnCity = returnCity;
        this.departureDate = departureDate;
    }

    public CityDTO getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(CityDTO departureCity) {
        this.departureCity = departureCity;
    }

    public CityDTO getReturnCity() {
        return returnCity;
    }

    public void setReturnCity(CityDTO returnCity) {
        this.returnCity = returnCity;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Override
    public String toString() {
        return "SearchFlightDTO{" +
                "departureCity=" + departureCity +
                ", returnCity=" + returnCity +
                ", departureDate=" + departureDate +
                '}';
    }
}
