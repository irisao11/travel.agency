package travel.agency.business.DTO.searchDTO;

import travel.agency.business.DTO.UserDTO;
import travel.agency.business.DTO.searchDTO.SearchTripDetailsDTO;

public class UserAndSearchDetailsDTO {
    private UserDTO userDTO;
    private SearchTripDetailsDTO searchTripDetailsDTO;

    public UserAndSearchDetailsDTO() {
    }

    public UserAndSearchDetailsDTO(UserDTO userDTO, SearchTripDetailsDTO searchTripDetailsDTO) {
        this.userDTO = userDTO;
        this.searchTripDetailsDTO = searchTripDetailsDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public SearchTripDetailsDTO getSearchTripDetailsDTO() {
        return searchTripDetailsDTO;
    }

    public void setSearchTripDetailsDTO(SearchTripDetailsDTO searchTripDetailsDTO) {
        this.searchTripDetailsDTO = searchTripDetailsDTO;
    }

    @Override
    public String toString() {
        return "UserAndSearchDetailsDTO{" +
                "userDTO=" + userDTO +
                ", searchTripDetailsDTO=" + searchTripDetailsDTO +
                '}';
    }
}
