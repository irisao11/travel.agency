package travel.agency.business.DTO.searchDTO;

import travel.agency.business.DTO.CityDTO;
import travel.agency.business.DTO.HotelDTO;

import java.sql.Date;

public class SearchTripDetailsDTO {
    private Date departureDate;
    private CityDTO departureCity;
    private Date returnDate;
    private CityDTO returnCity;
    private HotelDTO hotel;
    private Long numberOfPersons;

    public SearchTripDetailsDTO() {
    }

    public SearchTripDetailsDTO(Date departureDate, CityDTO departureCity, Date returnDate, CityDTO returnCity, HotelDTO hotel, Long numberOfPersons) {
        this.departureDate = departureDate;
        this.departureCity = departureCity;
        this.returnDate = returnDate;
        this.returnCity = returnCity;
        this.hotel = hotel;
        this.numberOfPersons = numberOfPersons;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public CityDTO getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(CityDTO departureCity) {
        this.departureCity = departureCity;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public CityDTO getReturnCity() {
        return returnCity;
    }

    public void setReturnCity(CityDTO returnCity) {
        this.returnCity = returnCity;
    }

    public HotelDTO getHotel() {
        return hotel;
    }

    public void setHotel(HotelDTO hotel) {
        this.hotel = hotel;
    }

    public Long getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(Long numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    @Override
    public String toString() {
        return "SearchTripDetailsDTO{" +
                "departureDate=" + departureDate +
                ", departureCity=" + departureCity +
                ", returnDate=" + returnDate +
                ", returnCity=" + returnCity +
                ", hotel=" + hotel +
                ", numberOfPersons=" + numberOfPersons +
                '}';
    }
}
