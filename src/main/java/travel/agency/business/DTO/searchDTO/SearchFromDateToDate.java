package travel.agency.business.DTO.searchDTO;

import java.sql.Date;

public class SearchFromDateToDate {
    private Date fromDate;
    private Date toDate;

    public SearchFromDateToDate() {
    }

    public SearchFromDateToDate(Date fromDate, Date toDate) {
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Override
    public String toString() {
        return "SearchFromDateToDate{" +
                "fromDate=" + fromDate +
                ", toDate=" + toDate +
                '}';
    }
}
