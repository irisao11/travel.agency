package travel.agency.business.DTO;

import java.sql.Date;

public class TripDetailsDTO {
    private UserDTO user;
    private TripDTO trip;
    private String number;
    private Date purchaseDate;
    private Long doubleRoomsBooked;
    private Long singleRoomsBooked;
    private Long extraBedsBooked;
    private Long amountSpent;

    public TripDetailsDTO() {
    }

    public TripDetailsDTO(UserDTO user, TripDTO trip, String number, Date purchaseDate, Long doubleRoomsBooked, Long singleRoomsBooked, Long extraBedsBooked, Long amountSpent) {
        this.user = user;
        this.trip = trip;
        this.number = number;
        this.purchaseDate = purchaseDate;
        this.doubleRoomsBooked = doubleRoomsBooked;
        this.singleRoomsBooked = singleRoomsBooked;
        this.extraBedsBooked = extraBedsBooked;
        this.amountSpent = amountSpent;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public TripDTO getTrip() {
        return trip;
    }

    public void setTrip(TripDTO trip) {
        this.trip = trip;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getDoubleRoomsBooked() {
        return doubleRoomsBooked;
    }

    public void setDoubleRoomsBooked(Long doubleRoomsBooked) {
        this.doubleRoomsBooked = doubleRoomsBooked;
    }

    public Long getSingleRoomsBooked() {
        return singleRoomsBooked;
    }

    public void setSingleRoomsBooked(Long singleRoomsBooked) {
        this.singleRoomsBooked = singleRoomsBooked;
    }

    public Long getExtraBedsBooked() {
        return extraBedsBooked;
    }

    public void setExtraBedsBooked(Long extraBedsBooked) {
        this.extraBedsBooked = extraBedsBooked;
    }

    public Long getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(Long amountSpent) {
        this.amountSpent = amountSpent;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    @Override
    public String toString() {
        return "TripDetailsDTO{" +
                "user=" + user +
                ", trip=" + trip +
                ", name='" + number + '\'' +
                ", doubleRoomsBooked=" + doubleRoomsBooked +
                ", singleRoomsBooked=" + singleRoomsBooked +
                ", extraBedsBooked=" + extraBedsBooked +
                ", amountSpent=" + amountSpent +
                ", purchaseDate="+purchaseDate +
                '}';
    }
}
