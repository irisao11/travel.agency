package travel.agency.business.DTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.sql.Date;

public class RoomDTO {
    private Date fromDate;
    private Date toDate;
    @Min(0) @Max(1000)
    private Long doubleRoomsAvailable;
    @Min(0)
    private Long priceForDouble;
    @Min(0)
    private Long singleRoomsAvailable;
    @Min(0)
    private Long priceForSingle;
    @Min(0)
    private Long extraBedsAvailable;
    @Min(0)
    private Long priceForExtra;
    private HotelDTO hotelDTO;

    public RoomDTO() {
    }

    public RoomDTO(Date fromDate, Date toDate, Long doubleRoomsAvailable, Long priceForDouble, Long singleRoomsAvailable, Long priceForSingle, Long extraBedsAvailable, Long priceForExtra, HotelDTO hotelDTO) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.doubleRoomsAvailable = doubleRoomsAvailable;
        this.priceForDouble = priceForDouble;
        this.singleRoomsAvailable = singleRoomsAvailable;
        this.priceForSingle = priceForSingle;
        this.extraBedsAvailable = extraBedsAvailable;
        this.priceForExtra = priceForExtra;
        this.hotelDTO = hotelDTO;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Long getDoubleRoomsAvailable() {
        return doubleRoomsAvailable;
    }

    public void setDoubleRoomsAvailable(Long doubleRoomsAvailable) {
        this.doubleRoomsAvailable = doubleRoomsAvailable;
    }

    public Long getPriceForDouble() {
        return priceForDouble;
    }

    public void setPriceForDouble(Long priceForDouble) {
        this.priceForDouble = priceForDouble;
    }

    public Long getSingleRoomsAvailable() {
        return singleRoomsAvailable;
    }

    public void setSingleRoomsAvailable(Long singleRoomsAvailable) {
        this.singleRoomsAvailable = singleRoomsAvailable;
    }

    public Long getPriceForSingle() {
        return priceForSingle;
    }

    public void setPriceForSingle(Long priceForSingle) {
        this.priceForSingle = priceForSingle;
    }

    public Long getExtraBedsAvailable() {
        return extraBedsAvailable;
    }

    public void setExtraBedsAvailable(Long extraBedsAvailable) {
        this.extraBedsAvailable = extraBedsAvailable;
    }

    public Long getPriceForExtra() {
        return priceForExtra;
    }

    public void setPriceForExtra(Long priceForExtra) {
        this.priceForExtra = priceForExtra;
    }

    public HotelDTO getHotelDTO() {
        return hotelDTO;
    }

    public void setHotelDTO(HotelDTO hotelDTO) {
        this.hotelDTO = hotelDTO;
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
                "fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", doubleRoomsAvailable=" + doubleRoomsAvailable +
                ", priceForDouble=" + priceForDouble +
                ", singleRoomsAvailable=" + singleRoomsAvailable +
                ", priceForSingle=" + priceForSingle +
                ", extraBedsAvailable=" + extraBedsAvailable +
                ", priceForExtra=" + priceForExtra +
                ", hotelDTO=" + hotelDTO +
                '}';
    }
}
