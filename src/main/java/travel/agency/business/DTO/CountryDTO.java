package travel.agency.business.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CountryDTO {

    @NotBlank
    @Pattern(regexp = "([a-zA-Z])*")
    private String name;
    private ContinentDTO continentDTO;


    public CountryDTO() {
    }

    public CountryDTO(String name, ContinentDTO continentDTO) {
        this.name = name;
        this.continentDTO = continentDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public ContinentDTO getContinentDTO() {
        return continentDTO;
    }

    public void setContinentDTO(ContinentDTO continentDTO) {
        this.continentDTO = continentDTO;
    }

    @Override
    public String toString() {
        return "CountryDTO{" +
                "name='" + name + '\'' +
                ", continentDTO=" + continentDTO +
                '}';
    }
}
