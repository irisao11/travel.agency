package travel.agency.business.DTO;

import javax.validation.constraints.Min;
import java.sql.Date;

public class FlightDTO {
    private String flightNumber;
    private Date departureDate;
    @Min(100)
    private int totalNumOfSeats;
    @Min(0)
    private int availableNumOfSeats;
    @Min(0)
    private Long price;
    private AirportDTO airportDTO;
    private AirportDTO returnAirportDTO;

    public FlightDTO() {
    }

    public FlightDTO(String flightNumber, Date departureDate, int totalNumOfSeats, int availableNumOfSeats, Long price, AirportDTO airportDTO, AirportDTO returnAirportDTO) {
        this.flightNumber = flightNumber;
        this.departureDate = departureDate;
        this.totalNumOfSeats = totalNumOfSeats;
        this.availableNumOfSeats = availableNumOfSeats;
        this.price = price;
        this.airportDTO = airportDTO;
        this.returnAirportDTO = returnAirportDTO;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public int getTotalNumOfSeats() {
        return totalNumOfSeats;
    }

    public void setTotalNumOfSeats(int totalNumOfSeats) {
        this.totalNumOfSeats = totalNumOfSeats;
    }

    public int getAvailableNumOfSeats() {
        return availableNumOfSeats;
    }

    public void setAvailableNumOfSeats(int availableNumOfSeats) {
        this.availableNumOfSeats = availableNumOfSeats;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public AirportDTO getAirportDTO() {
        return airportDTO;
    }

    public void setAirportDTO(AirportDTO airportDTO) {
        this.airportDTO = airportDTO;
    }

    public AirportDTO getReturnAirportDTO() {
        return returnAirportDTO;
    }

    public void setReturnAirportDTO(AirportDTO returnAirportDTO) {
        this.returnAirportDTO = returnAirportDTO;
    }

    @Override
    public String toString() {
        return "FlightDTO{" +
                "flightNumber='" + flightNumber + '\'' +
                ", departureDate=" + departureDate +
                ", totalNumOfSeats=" + totalNumOfSeats +
                ", availableNumOfSeats=" + availableNumOfSeats +
                ", price=" + price +
                ", airportDTO=" + airportDTO +
                ", returnAirportDTO=" + returnAirportDTO +
                '}';
    }
}
