package travel.agency.business.DTO;

public class AirportDTO {
    private String name;
    private CityDTO cityDTO;

    public AirportDTO() {
    }

    public AirportDTO(String name, CityDTO cityDTO) {
        this.name = name;
        this.cityDTO = cityDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    @Override
    public String toString() {
        return "AirportDTO{" +
                "name='" + name + '\'' +
                ", cityDTO=" + cityDTO +
                '}';
    }
}
