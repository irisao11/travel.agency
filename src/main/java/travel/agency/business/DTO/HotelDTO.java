package travel.agency.business.DTO;

public class HotelDTO {
    private String name;
    private String hotelDescription;
    private int standard;
    private CityDTO cityDTO;

    public HotelDTO() {
    }

    public HotelDTO(String name, String hotelDescription, int standard, CityDTO cityDTO) {
        this.name = name;
        this.hotelDescription = hotelDescription;
        this.standard = standard;
        this.cityDTO = cityDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHotelDescription() {
        return hotelDescription;
    }

    public void setHotelDescription(String hotelDescription) {
        this.hotelDescription = hotelDescription;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    @Override
    public String toString() {
        return "HotelDTO{" +
                "name='" + name + '\'' +
                ", hotelDescription='" + hotelDescription + '\'' +
                ", standard=" + standard +
                ", cityDTO=" + cityDTO +
                '}';
    }
}
