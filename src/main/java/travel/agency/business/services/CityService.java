package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.CityDTO;
import travel.agency.business.DTO.CountryDTO;
import travel.agency.persistance.DAO.CityDAO;
import travel.agency.persistance.DAO.CountryDAO;
import travel.agency.persistance.entities.City;
import travel.agency.persistance.entities.Country;

import java.util.LinkedList;
import java.util.List;

@Service
public class CityService {
    @Autowired
    CityDAO cityDAO;
    @Autowired
    CountryService countryService;

    public void addCity(CityDTO cityDTO){
        City city = new City();
        city.setName(cityDTO.getName());
        Country country = countryService.findCountryByName(cityDTO.getCountryDTO());
        city.setCountry(country);
        cityDAO.insertCity(city);
    }

    public List<CityDTO> findListOfCities(){
        List<CityDTO> cityDTOS = new LinkedList<>();
        List<City> cityList = cityDAO.findListOfCities();

        for(City c: cityList){
            CityDTO cityDTO = new CityDTO();
            cityDTO.setName(c.getName());
            Country country = c.getCountry();
            CountryDTO countryDTO = new CountryDTO();
            countryDTO.setName(country.getName());
            cityDTO.setCountryDTO(countryDTO);
            cityDTOS.add(cityDTO);
        }
        return cityDTOS;
    }

    public City findCityByName(CityDTO cityDTO){
        City city = cityDAO.findCityByName(cityDTO.getName());
        return city;
    }

    public CityDTO findCityDTOByName(String name){
        City city = cityDAO.findCityByName(name);

        CityDTO cityDTO = new CityDTO();
        if(city != null){
            cityDTO.setName(city.getName());
            //cityDTO.setCountryDTO();
        }else{
            System.out.println("City name is not valid");
        }
        System.out.println("CITYDTO"+cityDTO);
        return cityDTO;
    }

}
