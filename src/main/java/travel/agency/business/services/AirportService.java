package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.AirportDTO;
import travel.agency.business.DTO.CityDTO;
import travel.agency.persistance.DAO.AirportDAO;
import travel.agency.persistance.entities.Airport;
import travel.agency.persistance.entities.City;

import java.util.LinkedList;
import java.util.List;

@Service
public class AirportService {
    @Autowired
    AirportDAO airportDAO;
    @Autowired
    CityService cityService;

    public void addAirport(AirportDTO airportDTO){
        Airport airport = new Airport();
        airport.setName(airportDTO.getName());
        CityDTO cityDTO = airportDTO.getCityDTO();
        City city = cityService.findCityByName(cityDTO);
        airport.setCity(city);
        airportDAO.insertAirport(airport);
    }

    public Airport findAirportByName(AirportDTO airportDTO){
        Airport airport = airportDAO.findAirportByName(airportDTO.getName());
    return airport;
    }



    public AirportDTO findAirportDTO(String name){
        Airport airport = airportDAO.findAirportByName(name);
        AirportDTO airportDTO = new AirportDTO();
        if(airport != null){
            airportDTO.setName(airport.getName());
            CityDTO cityDTO = cityService.findCityDTOByName(airport.getCity().getName());
            airportDTO.setCityDTO(cityDTO);
        }else{
            System.out.println("Did not find that airport");
        }
    return airportDTO;
    }

    public List<AirportDTO> findAirportsInCity(CityDTO cityDTO){
        City city = new City();
        city.setName(cityDTO.getName());
        List<AirportDTO> airportDTOSByCity = new LinkedList<>();
        if(city != null){
            List<Airport> airportsInCity = airportDAO.findAirportsByCity(city);
        for(Airport a: airportsInCity){
            AirportDTO airportDTO = new AirportDTO();
            airportDTO.setName(a.getName());
            airportDTO.setCityDTO(cityDTO);
        airportDTOSByCity.add(airportDTO);
        }
        }else{
            System.out.println("Try another city");
        }
    return airportDTOSByCity;
    }
}
