package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.ContinentDTO;
import travel.agency.business.DTO.CountryDTO;
import travel.agency.persistance.DAO.CountryDAO;
import travel.agency.persistance.entities.Continent;
import travel.agency.persistance.entities.Country;

import java.util.LinkedList;
import java.util.List;

@Service
public class CountryService {
    @Autowired
    CountryDAO countryDAO;
    @Autowired
    ContinentService continentService;

    public void addCountry(CountryDTO countryDTO){
        Country country = new Country();
        country.setName(countryDTO.getName());
        ContinentDTO continentDTO = countryDTO.getContinentDTO();
        Continent continent = continentService.findContinentByName(continentDTO);
        country.setContinent(continent);
        countryDAO.insertCountry(country);
    }

    public List<CountryDTO> findListOfCCountries(){
        List<CountryDTO> countryDTOS = new LinkedList<>();
        List<Country> countryList = countryDAO.findCountries();

        for(Country c:countryList){
            CountryDTO countryDTO = new CountryDTO();
            countryDTO.setName(c.getName());
            Continent continent = c.getContinent();
            ContinentDTO continentDTO = new ContinentDTO();
            continentDTO.setName(continent.getName());
            countryDTO.setContinentDTO(continentDTO);
            countryDTOS.add(countryDTO);
        }
        return countryDTOS;
    }

    public Country findCountryByName(CountryDTO countryDTO){
        Country country = countryDAO.findCountryByName(countryDTO.getName());
        return country;
    }


}
