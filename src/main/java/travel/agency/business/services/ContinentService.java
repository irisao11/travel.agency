package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.ContinentDTO;
import travel.agency.persistance.DAO.ContinentDAO;
import travel.agency.persistance.entities.Continent;

import java.util.LinkedList;
import java.util.List;

@Service
public class ContinentService {
    @Autowired
    private ContinentDAO continentDAO;

    public List<ContinentDTO> findListOfContinents(){
        List<ContinentDTO> continentDTOS = new LinkedList<>();
        List<Continent> continentList = continentDAO.findListOfContinents();

        for(Continent c:continentList){
            ContinentDTO continentDTO = new ContinentDTO();
            continentDTO.setName(c.getName());
            continentDTOS.add(continentDTO);
        }
        return continentDTOS;
    }

    public boolean verifyIfContinent(ContinentDTO continentDTO){
        boolean continentExists = false;

        List<Continent>continentList = continentDAO.findListOfContinents();
        for(Continent c: continentList){
            if(continentDTO.equals(c)){
                continentExists = true;
                break;
            }else{
                System.out.println("Please insert a valid continent");
            }
        }
        return continentExists;
    }

    public Continent verifyContinent(ContinentDTO continentDTO) {
        List<Continent> continentList = continentDAO.findListOfContinents();
        Continent continent = new Continent();
        for (Continent c : continentList) {
            if (continentDTO.getName().equals(c.getName())) {
                continent = c;
                break;
            } else {
                System.out.println("Please insert a valid continent");
            }
        }
        return continent;
    }

    public Continent findContinentByName(ContinentDTO continentDTO){
        Continent continent = continentDAO.findContinentByName(continentDTO.getName());
        return continent;
    }
}
