package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.UserDTO;
import travel.agency.business.services.login.SecurePassword;
import travel.agency.persistance.DAO.UserDAO;
import travel.agency.persistance.entities.User;

@Service
public class UserService {
    @Autowired
    private UserDAO userDAO;

    private String salt = SecurePassword.generateSalt(12).get();

    public void addUser(UserDTO userDTO){
        boolean isLoginSuccessful = false;
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setUserName(userDTO.getUserName());
        user.setEmail(userDTO.getEmail());

        user.setSalt(salt);
        String hashedPassword = SecurePassword.hashPassword(userDTO.getPassword(), salt).get();
        user.setPassword(hashedPassword);
        userDAO.insertUser(user);
    }

    public UserDTO findUserByNameAndPassword(UserDTO userDTO){
        boolean isValid = validateCredentialsOne(userDTO.getUserName(), userDTO.getPassword());
          UserDTO validUser = new UserDTO();
        if(isValid){
          validUser.setUserName(userDTO.getUserName());
          validUser.setPassword(userDTO.getPassword());
        }
        return validUser;
    }

    public boolean validateCredentials(String userName, String password) {
        boolean isValid = false;
        User loginUser = userDAO.findUserByNameAndPassword(userName, password);
        if (loginUser != null) {
            System.out.println("Login successful");
            isValid = true;
        } else {
            System.out.println("wrong user or password");
        }
        return isValid;
    }

    public boolean validateCredentialsOne(String userName, String password) {
        String saltFromDb = userDAO.findSaltNamedQuery(userName);
        boolean isValid = false;
        String hashedPassword = SecurePassword.hashPassword(password, saltFromDb).get();
        String passwordFromDB = userDAO.findPasswordNamedQuery(userName);
        if (userDAO.findUserByName(userName)) {
            if (hashedPassword.equals(passwordFromDB)) {
                System.out.println("Login successful");
                isValid = true;
            } else {
                System.out.println("Wrong user or password");
            }
        } else {
            System.out.println("wrong user or password");
        }
        return isValid;
    }

    public boolean validateCredentialsAndAdmin(String userName, String password) {
        String saltFromDb = userDAO.findSaltNamedQuery(userName);
        boolean isValid = false;
        String hashedPassword = SecurePassword.hashPassword(password, saltFromDb).get();
        String passwordFromDB = userDAO.findPasswordNamedQuery(userName);
        if (userDAO.findUserByName(userName)) {
            if (hashedPassword.equals(passwordFromDB)) {
                System.out.println("Login successful");
                if(userDAO.verifyADMIN(userName)){
                    System.out.println("User is Admin");
                    isValid = true;
                }else{
                    System.out.println("user is not admin");
                    isValid = false;
                }
            } else {
                System.out.println("Wrong user or password");
            }
        } else {
            System.out.println("wrong user or password");
        }
        return isValid;
    }

}
