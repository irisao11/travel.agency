package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.*;
import travel.agency.business.DTO.searchDTO.SearchFromDateToDate;
import travel.agency.business.DTO.searchDTO.SearchTripByDatesDTO;
import travel.agency.business.DTO.searchDTO.SearchTripDetailsDTO;
import travel.agency.business.DTO.searchDTO.SearchUserAndTripDTO;
import travel.agency.persistance.DAO.*;
import travel.agency.persistance.entities.*;

import java.util.LinkedList;
import java.util.List;

@Service
public class TripService {
    @Autowired
    TripDAO tripDAO;
    @Autowired
    FlightService flightService;
    @Autowired
    FlightDAO flightDAO;
    @Autowired
    HotelService hotelService;
    @Autowired
    HotelDAO hotelDAO;
    @Autowired
    RoomDAO roomDAO;
    @Autowired
    UserService userService;
    @Autowired
    AirportService airportService;
    @Autowired
    AirportDAO airportDAO;
    @Autowired
    CityService cityService;
    @Autowired
    CityDAO cityDAO;
    @Autowired
    CountryService countryService;
    @Autowired
    CountryDAO countryDAO;
    @Autowired
    ContinentService continentService;


    public boolean addTrip(SearchUserAndTripDTO searchUserAndTripDTO) {
        boolean tripAdded = false;
        if (userService.validateCredentialsAndAdmin(searchUserAndTripDTO.getUserDTO().getUserName(), searchUserAndTripDTO.getUserDTO().getPassword())) {

            Trip trip = new Trip();

            FlightDTO departureFlightDTO = searchUserAndTripDTO.getTripDTO().getDepartureFlightDTO();
            Flight departureFlight = new Flight();

            if (flightDAO.verifyIfFlightByNumber(departureFlightDTO.getFlightNumber())) {
                departureFlight = flightService.findFlightByNumber(departureFlightDTO.getFlightNumber());
                trip.setDepartureFlight(departureFlight);
            } else {
                departureFlight.setFlightNumber(departureFlightDTO.getFlightNumber());
                departureFlight.setDepartureDate(departureFlightDTO.getDepartureDate());
                departureFlight.setTotalNumOfSeats(departureFlightDTO.getTotalNumOfSeats());
                departureFlight.setAvailableNumOfSeats(departureFlightDTO.getAvailableNumOfSeats());
                departureFlight.setFlightPrice(departureFlightDTO.getPrice());

                AirportDTO airportDTO = departureFlightDTO.getAirportDTO();

                Airport airport = new Airport();
                airport.setName(airportDTO.getName());
                boolean existingAirport = airportDAO.verifyAirportByName(airport.getName());
                if (existingAirport) {
                    airport = airportDAO.findAirportByName(airport.getName());
                    departureFlight.setAirport(airport);
                } else {
                    CityDTO cityDTO = airportDTO.getCityDTO();
                    City city = new City();
                    city.setName(cityDTO.getName());
                    boolean existingCity = cityDAO.verifyCityByName(cityDTO.getName());
                    if (existingCity) {
                        city = cityDAO.findCityByName(cityDTO.getName());
                        airport.setCity(city);
                        departureFlight.setAirport(airport);
                    } else {
                        CountryDTO countryDTO = cityDTO.getCountryDTO();
                        Country country = new Country();

                        boolean existingCountry = countryDAO.verifyCountryByName(countryDTO.getName());
                        if (existingCountry) {
                            country = countryDAO.findCountryByName(countryDTO.getName());
                            System.out.println("country" + country);
                            city.setCountry(country);
                            airport.setCity(city);
                            departureFlight.setAirport(airport);
                        } else {
                            ContinentDTO continentDTO = countryDTO.getContinentDTO();
                            Continent continent = continentService.findContinentByName(continentDTO);
                            country.setContinent(continent);
                            countryService.addCountry(countryDTO);
                            city.setCountry(country);
                            cityService.addCity(cityDTO);
                            airport.setCity(city);
                            airportService.addAirport(airportDTO);
                            departureFlight.setAirport(airport);

                        }
                    }
                }

                AirportDTO returnAirportDTO = departureFlightDTO.getReturnAirportDTO();
                Airport returnAirport = new Airport();
                returnAirport.setName(returnAirportDTO.getName());
                boolean existingAirportOne = airportDAO.verifyAirportByName(returnAirport.getName());
                if (existingAirportOne) {
                    returnAirport = airportDAO.findAirportByName(returnAirportDTO.getName());
                    departureFlight.setReturnAirport(returnAirport);
                } else {
                    CityDTO returnCityDTO = returnAirportDTO.getCityDTO();
                    City returnCity = new City();
                    returnCity.setName(returnCityDTO.getName());
                    boolean existingCity = cityDAO.verifyCityByName(returnCityDTO.getName());
                    if (existingCity) {
                        returnCity = cityDAO.findCityByName(returnCityDTO.getName());
                        returnAirport.setCity(returnCity);
                        departureFlight.setReturnAirport(returnAirport);
                    } else {
                        CountryDTO returnCountryDTO = returnCityDTO.getCountryDTO();
                        System.out.println("returnCountry" + returnCountryDTO);
                        Country returnCountry = new Country();
                        returnCountry.setName(returnCountryDTO.getName());
                        boolean existingCountry = countryDAO.verifyCountryByName(returnCountryDTO.getName());
                        if (existingCountry) {
                            returnCountry = countryDAO.findCountryByName(returnCountryDTO.getName());
                            returnCity.setCountry(returnCountry);
                            returnAirport.setCity(returnCity);
                            departureFlight.setReturnAirport(returnAirport);
                        } else {
                            ContinentDTO continentDTO = returnCountryDTO.getContinentDTO();
                            Continent continent = continentService.findContinentByName(continentDTO);
                            returnCountry.setContinent(continent);
                            countryService.addCountry(returnCountryDTO);
                            returnCity.setCountry(returnCountry);
                            cityService.addCity(returnCityDTO);
                            returnAirport.setCity(returnCity);
                            airportService.addAirport(returnAirportDTO);
                            departureFlight.setReturnAirport(returnAirport);
                        }
                    }
                }
                flightService.addFlight(departureFlightDTO);
                Flight depFlight = flightDAO.findFlightByNumber(departureFlightDTO.getFlightNumber());
                trip.setDepartureFlight(depFlight);
            }

            FlightDTO returnFlightDTO = searchUserAndTripDTO.getTripDTO().getReturnFlightDTO();
            Flight returnFlight = new Flight();
            boolean existingRetFlight = flightDAO.verifyIfFlightByNumber(returnFlightDTO.getFlightNumber());
            if (existingRetFlight) {
                returnFlight = flightDAO.findFlightByNumber(returnFlightDTO.getFlightNumber());
                trip.setReturnFlight(returnFlight);
            } else {
                returnFlight.setFlightNumber(returnFlightDTO.getFlightNumber());
                returnFlight.setDepartureDate(returnFlightDTO.getDepartureDate());
                returnFlight.setTotalNumOfSeats(returnFlightDTO.getTotalNumOfSeats());
                returnFlight.setAvailableNumOfSeats(returnFlightDTO.getAvailableNumOfSeats());
                returnFlight.setFlightPrice(returnFlightDTO.getPrice());

                AirportDTO fromAirportDTO = returnFlightDTO.getAirportDTO();

                Airport airport = new Airport();
                airport.setName(fromAirportDTO.getName());
                boolean existingAirport = airportDAO.verifyAirportByName(fromAirportDTO.getName());
                if (existingAirport) {
                    airport = airportDAO.findAirportByName(fromAirportDTO.getName());
                    returnFlight.setAirport(airport);
                } else {
                    CityDTO cityDTO = fromAirportDTO.getCityDTO();
                    City city = new City();
                    city.setName(cityDTO.getName());
                    boolean existingCity = cityDAO.verifyCityByName(cityDTO.getName());
                    if (existingCity) {
                        city = cityDAO.findCityByName(cityDTO.getName());
                        airport.setCity(city);
                        returnFlight.setAirport(airport);
                    } else {
                        CountryDTO countryDTO = cityDTO.getCountryDTO();
                        Country country = new Country();
                        country.setName(countryDTO.getName());
                        boolean existingCountry = countryDAO.verifyCountryByName(countryDTO.getName());
                        if (existingCountry) {
                            country = countryDAO.findCountryByName(countryDTO.getName());
                            city.setCountry(country);
                            airport.setCity(city);
                            returnFlight.setAirport(airport);
                        } else {
                            ContinentDTO continentDTO = countryDTO.getContinentDTO();
                            Continent continent = continentService.findContinentByName(continentDTO);
                            country.setContinent(continent);
                            city.setCountry(country);
                            airport.setCity(city);
                            returnFlight.setAirport(airport);
                        }
                    }
                }
                    AirportDTO toAirportDTO = returnFlightDTO.getReturnAirportDTO();
                    Airport toAirport = new Airport();
                    toAirport.setName(toAirportDTO.getName());
                    boolean existingToAirport = airportDAO.verifyAirportByName(toAirportDTO.getName());
                    if (existingToAirport) {
                        toAirport = airportDAO.findAirportByName(toAirportDTO.getName());
                        returnFlight.setReturnAirport(toAirport);
                    } else {
                        CityDTO toCityDTO = toAirportDTO.getCityDTO();
                        City toCity = new City();
                        toCity.setName(toCityDTO.getName());
                        boolean existingToCity = cityDAO.verifyCityByName(toCityDTO.getName());
                        if (existingToCity) {
                            toCity = cityDAO.findCityByName(toCityDTO.getName());
                            toAirport.setCity(toCity);
                            returnFlight.setReturnAirport(toAirport);
                        } else {
                            CountryDTO toCountryDTO = toCityDTO.getCountryDTO();
                            Country toCountry = new Country();
                            toCountry.setName(toCountryDTO.getName());
                            boolean existingToCountry = countryDAO.verifyCountryByName(toCountryDTO.getName());
                            if (existingToCountry) {
                                toCountry = countryDAO.findCountryByName(toCountryDTO.getName());
                                toCity.setCountry(toCountry);
                                toAirport.setCity(toCity);
                                returnFlight.setReturnAirport(toAirport);
                            } else {
                                ContinentDTO continentDTO = toCountryDTO.getContinentDTO();
                                Continent toContinent = continentService.findContinentByName(continentDTO);
                                toCountry.setContinent(toContinent);
                                toCity.setCountry(toCountry);
                                toAirport.setCity(toCity);
                                returnFlight.setReturnAirport(toAirport);

                            }
                        }

                    }
                    flightService.addFlight(returnFlightDTO);
                    Flight retFlight = flightDAO.findFlightByNumber(returnFlightDTO.getFlightNumber());
                    trip.setReturnFlight(retFlight);
                }

                HotelDTO hotelDTO = searchUserAndTripDTO.getTripDTO().getHotelDTO();
                Hotel hotel = new Hotel();
                hotel.setName(hotelDTO.getName());
                City city = new City();
                city.setName(hotelDTO.getCityDTO().getName());

                boolean existingHotel = hotelDAO.verifyHotelByName(searchUserAndTripDTO.getTripDTO().getHotelDTO().getName(), searchUserAndTripDTO.getTripDTO().getHotelDTO().getCityDTO().getName());
                if (existingHotel) {
                    hotel = hotelDAO.findHotelByName(hotelDTO.getName(), hotelDTO.getCityDTO().getName());
                    trip.setHotel(hotel);
                    hotel.setName(searchUserAndTripDTO.getTripDTO().getHotelDTO().getName());
                } else {
                    hotel.setStandard(searchUserAndTripDTO.getTripDTO().getHotelDTO().getStandard());
                    hotel.setHotelDescription(searchUserAndTripDTO.getTripDTO().getHotelDTO().getHotelDescription());
                    CityDTO hotelcityDTO = searchUserAndTripDTO.getTripDTO().getHotelDTO().getCityDTO();
                    City hotelcity = new City();
                    hotelcity.setName(hotelcityDTO.getName());
                    boolean existingHotelCity = cityDAO.verifyCityByName(hotelcityDTO.getName());
                    if (existingHotelCity) {
                        hotelcity = cityDAO.findCityByName(hotelcityDTO.getName());
                        hotel.setCity(hotelcity);

                    } else {
                        CountryDTO countryHotelDTO = hotelcityDTO.getCountryDTO();
                        Country hotelCountry = new Country();
                        hotelCountry.setName(countryHotelDTO.getName());
                        boolean existingHotelCountry = countryDAO.verifyCountryByName(countryHotelDTO.getName());
                        if (existingHotelCountry) {
                            hotelCountry = countryDAO.findCountryByName(countryHotelDTO.getName());
                            hotelcity.setCountry(hotelCountry);
                            hotel.setCity(hotelcity);
                        } else {
                            ContinentDTO continentDTO = countryHotelDTO.getContinentDTO();
                            Continent hotelContinent = continentService.findContinentByName(continentDTO);
                            hotelCountry.setContinent(hotelContinent);
                            hotelcity.setCountry(hotelCountry);
                            hotel.setCity(hotelcity);

                        }
                    }
                    hotelService.addHotel(hotelDTO);
                    Hotel thisHotel = hotelDAO.findHotelByName(hotelDTO.getName(), hotelDTO.getCityDTO().getName());
                    trip.setHotel(thisHotel);
                }

                trip.setPromoted(searchUserAndTripDTO.getTripDTO().isPromoted());
                tripDAO.addTrip(trip);
                tripAdded = true;

            }else{
                System.out.println("you are not allowed to make this operation");
            }
            return tripAdded;
        }



    public TripDTO findTripToCityFromDateToDateAndNumOfPersons(SearchTripDetailsDTO searchTripDetailsDTO) {
        TripDTO chosenTripDTO = new TripDTO();

        Trip chosenTrip = tripDAO.findTrips(searchTripDetailsDTO.getDepartureDate(),
                                                            searchTripDetailsDTO.getDepartureCity().getName(),
                                                            searchTripDetailsDTO.getReturnDate(),
                                                            searchTripDetailsDTO.getReturnCity().getName(),
                                                            searchTripDetailsDTO.getHotel().getName());
        Long numOfBedsInHotel = roomDAO.countNumberOfBedsAvailableForHotelAndPeriod(chosenTrip.getHotel().getName(), chosenTrip.getHotel().getCity().getName(), chosenTrip.getDepartureFlight().getDepartureDate(), chosenTrip.getReturnFlight().getDepartureDate());

        if (chosenTrip.getDepartureFlight().getAvailableNumOfSeats() >= searchTripDetailsDTO.getNumberOfPersons()
                && chosenTrip.getReturnFlight().getAvailableNumOfSeats() >= searchTripDetailsDTO.getNumberOfPersons()
                && numOfBedsInHotel>= searchTripDetailsDTO.getNumberOfPersons()) {

                    Flight flight = chosenTrip.getDepartureFlight();
                    FlightDTO flightDTO = new FlightDTO();

                    flightDTO.setFlightNumber(flight.getFlightNumber());
                    flightDTO.setPrice(flight.getFlightPrice());
                    flightDTO.setTotalNumOfSeats(flight.getTotalNumOfSeats());
                    flightDTO.setAvailableNumOfSeats(flight.getAvailableNumOfSeats());
                    flightDTO.setDepartureDate(flight.getDepartureDate());

                        Airport airport = flight.getAirport();
                        AirportDTO airportDTO = new AirportDTO();
                        airportDTO.setName(airport.getName());
                        City city = airport.getCity();
                        CityDTO cityDTO = new CityDTO();
                        cityDTO.setName(city.getName());
                        Country country = city.getCountry();
                        CountryDTO countryDTO = new CountryDTO();
                        countryDTO.setName(country.getName());
                        cityDTO.setCountryDTO(countryDTO);
                        airportDTO.setCityDTO(cityDTO);
                        flightDTO.setAirportDTO(airportDTO);

                        Airport returnAirport = flight.getReturnAirport();
                        AirportDTO returnAirportDTO = new AirportDTO();
                        returnAirportDTO.setName(returnAirport.getName());
                        City returnCity = returnAirport.getCity();
                        CityDTO returnCityDTO = new CityDTO();
                        returnCityDTO.setName(returnCity.getName());
                        Country returnCountry = returnCity.getCountry();
                        CountryDTO returnCountryDTO = new CountryDTO();
                        returnCountryDTO.setName(returnCountry.getName());
                        returnCityDTO.setCountryDTO(returnCountryDTO);
                        returnAirportDTO.setCityDTO(returnCityDTO);
                        flightDTO.setReturnAirportDTO(returnAirportDTO);
        chosenTripDTO.setDepartureFlightDTO(flightDTO);

                    Flight returnFlight = chosenTrip.getReturnFlight();
                    FlightDTO returnFlightDTO = new FlightDTO();

                    returnFlightDTO.setFlightNumber(returnFlight.getFlightNumber());
                    returnFlightDTO.setDepartureDate(returnFlight.getDepartureDate());
                    returnFlightDTO.setTotalNumOfSeats(returnFlight.getTotalNumOfSeats());
                    returnFlightDTO.setAvailableNumOfSeats(returnFlight.getAvailableNumOfSeats());
                    returnFlightDTO.setPrice(returnFlight.getFlightPrice());

                        Airport airportReturn = returnFlight.getAirport();
                        AirportDTO airportReturnDTO = new AirportDTO();
                        airportReturnDTO.setName(airportReturn.getName());
                            City cityReturn = airportReturn.getCity();
                            CityDTO cityReturnDTO = new CityDTO();
                            cityReturnDTO.setName(cityReturn.getName());
                                Country countryReturn = cityReturn.getCountry();
                                CountryDTO countryReturnDTO = new CountryDTO();
                                countryReturnDTO.setName(countryReturn.getName());
                            cityReturnDTO.setCountryDTO(countryReturnDTO);
                        airportReturnDTO.setCityDTO(cityReturnDTO);
                    returnFlightDTO.setAirportDTO(airportReturnDTO);

                        Airport homeAirport = returnFlight.getReturnAirport();
                        AirportDTO homeAirportDTO = new AirportDTO();
                        homeAirportDTO.setName(homeAirport.getName());
                            City homeCity = homeAirport.getCity();
                            CityDTO homeCityDTO = new CityDTO();
                            homeCityDTO.setName(homeCity.getName());
                                Country homeCountry = homeCity.getCountry();
                                CountryDTO homeCountryDTO = new CountryDTO();
                                homeCountryDTO.setName(homeCountry.getName());
                            homeCityDTO.setCountryDTO(homeCountryDTO);
                        homeAirportDTO.setCityDTO(homeCityDTO);
                    returnFlightDTO.setReturnAirportDTO(homeAirportDTO);
        chosenTripDTO.setReturnFlightDTO(returnFlightDTO);

            Hotel hotel = chosenTrip.getHotel();
            HotelDTO hotelDTO = new HotelDTO();

            hotelDTO.setName(hotel.getName());
            hotelDTO.setHotelDescription(hotel.getHotelDescription());
            hotelDTO.setStandard(hotel.getStandard());
            City hotelCity = hotel.getCity();
            CityDTO hotelCityDTO = new CityDTO();
            hotelCityDTO.setName(hotelCity.getName());
            hotelDTO.setCityDTO(hotelCityDTO);


        chosenTripDTO.setHotelDTO(hotelDTO);

        } else {
            System.out.println("There are no available seats");
        }
        return chosenTripDTO;
    }

    public List<TripDTO> findListOfTripsByDetails(SearchTripByDatesDTO searchTripByDatesDTO) {
        List<TripDTO> listOfTripDTOByDetails = new LinkedList<>();
        Long numOfTripsByDetails = tripDAO.countTripsByDetails(searchTripByDatesDTO.getDepartureDate(), searchTripByDatesDTO.getReturnDate(), searchTripByDatesDTO.getDepartureCity().getName(), searchTripByDatesDTO.getReturnCity().getName());
        if (numOfTripsByDetails != null) {
            List<Trip> listOfTripsByDetails = tripDAO.findListOfTripsByTripDetails(searchTripByDatesDTO.getDepartureDate(), searchTripByDatesDTO.getReturnDate(), searchTripByDatesDTO.getDepartureCity().getName(), searchTripByDatesDTO.getReturnCity().getName());
            for (Trip t : listOfTripsByDetails) {
                TripDTO tripDTO = new TripDTO();

                Flight flight = t.getDepartureFlight();
                FlightDTO flightDTO = new FlightDTO();
                flightDTO.setFlightNumber(flight.getFlightNumber());
                flightDTO.setTotalNumOfSeats(flight.getTotalNumOfSeats());
                flightDTO.setAvailableNumOfSeats(flight.getAvailableNumOfSeats());
                flightDTO.setPrice(flight.getFlightPrice());
                flightDTO.setDepartureDate(flight.getDepartureDate());

                Airport airport = flight.getAirport();
                AirportDTO airportDTO = new AirportDTO();
                airportDTO.setName(airport.getName());
                City city = airport.getCity();
                CityDTO cityDTO = new CityDTO();
                cityDTO.setName(city.getName());
                Country country = city.getCountry();
                CountryDTO countryDTO = new CountryDTO();
                countryDTO.setName(country.getName());
                cityDTO.setCountryDTO(countryDTO);
                airportDTO.setCityDTO(cityDTO);
                flightDTO.setAirportDTO(airportDTO);

                Airport returnAirport = flight.getReturnAirport();
                AirportDTO returnAirportDTO = new AirportDTO();
                returnAirportDTO.setName(returnAirport.getName());
                City returnCity = returnAirport.getCity();
                CityDTO returnCityDTO = new CityDTO();
                returnCityDTO.setName(returnCity.getName());
                Country returnCountry = returnCity.getCountry();
                CountryDTO returnCountryDTO = new CountryDTO();
                returnCountryDTO.setName(returnCountry.getName());
                returnCityDTO.setCountryDTO(returnCountryDTO);
                returnAirportDTO.setCityDTO(returnCityDTO);
                flightDTO.setReturnAirportDTO(returnAirportDTO);


                Flight returnFlight = t.getReturnFlight();
                FlightDTO returnFlightDTO = new FlightDTO();
                returnFlightDTO.setFlightNumber(returnFlight.getFlightNumber());
                returnFlightDTO.setDepartureDate(returnFlight.getDepartureDate());
                returnFlightDTO.setTotalNumOfSeats(returnFlight.getTotalNumOfSeats());
                returnFlightDTO.setAvailableNumOfSeats(returnFlight.getAvailableNumOfSeats());
                returnFlightDTO.setPrice(returnFlight.getFlightPrice());

                Airport airportReturn = returnFlight.getAirport();
                AirportDTO airportReturnDTO = new AirportDTO();
                airportReturnDTO.setName(airportReturn.getName());
                City cityReturn = airportReturn.getCity();
                CityDTO cityReturnDTO = new CityDTO();
                cityReturnDTO.setName(cityReturn.getName());
                Country countryReturn = cityReturn.getCountry();
                CountryDTO countryReturnDTO = new CountryDTO();
                countryReturnDTO.setName(countryReturn.getName());
                cityReturnDTO.setCountryDTO(countryReturnDTO);
                airportReturnDTO.setCityDTO(cityReturnDTO);
                returnFlightDTO.setAirportDTO(airportReturnDTO);

                Airport homeAirport = returnFlight.getReturnAirport();
                AirportDTO homeAirportDTO = new AirportDTO();
                homeAirportDTO.setName(homeAirport.getName());
                City homeCity = homeAirport.getCity();
                CityDTO homeCityDTO = new CityDTO();
                homeCityDTO.setName(homeCity.getName());
                Country homeCountry = homeCity.getCountry();
                CountryDTO homeCountryDTO = new CountryDTO();
                homeCountryDTO.setName(homeCountry.getName());
                homeCityDTO.setCountryDTO(homeCountryDTO);
                homeAirportDTO.setCityDTO(homeCityDTO);
                returnFlightDTO.setReturnAirportDTO(homeAirportDTO);

                tripDTO.setDepartureFlightDTO(flightDTO);
                tripDTO.setReturnFlightDTO(returnFlightDTO);

                Hotel hotel = t.getHotel();
                HotelDTO hotelDTO = new HotelDTO();
                hotelDTO.setName(hotel.getName());
                hotelDTO.setHotelDescription(hotel.getHotelDescription());
                hotelDTO.setStandard(hotel.getStandard());
                City hotelCity = hotel.getCity();
                CityDTO hotelCityDTO = new CityDTO();
                hotelCityDTO.setName(hotelCity.getName());
                hotelDTO.setCityDTO(hotelCityDTO);

                tripDTO.setHotelDTO(hotelDTO);
                Long numOfBedsInHotel = roomDAO.countNumberOfBedsAvailableForHotelAndPeriod(tripDTO.getHotelDTO().getName(),
                                                                                            tripDTO.getHotelDTO().getCityDTO().getName(),
                                                                                            tripDTO.getDepartureFlightDTO().getDepartureDate(),
                                                                                            tripDTO.getReturnFlightDTO().getDepartureDate());
                tripDTO.setPromoted(t.isPromoted());

                if (flightDTO.getAvailableNumOfSeats() >= searchTripByDatesDTO.getNumberOfPersons()
                 && returnFlightDTO.getAvailableNumOfSeats() >= searchTripByDatesDTO.getNumberOfPersons()
                 && numOfBedsInHotel>= searchTripByDatesDTO.getNumberOfPersons()) {

                    listOfTripDTOByDetails.add(tripDTO);
                } else {
                    System.out.println("There are no available seats");
                }
            }
            }else{
                System.out.println("there are no flights for these dates");
        }
            return listOfTripDTOByDetails;

    }

    public List<TripDTO> sortByCountryDTO(){
        List<TripDTO> sortedByCountryTripsDTO = new LinkedList<>();
        List<Trip> sortedByCountryList = tripDAO.sortTripsByCountry();
        for(Trip t: sortedByCountryList){
            TripDTO tripDTO = tripToTripDTO(t);
            sortedByCountryTripsDTO.add(tripDTO);
        }
    return sortedByCountryTripsDTO;
    }

    public List<TripDTO> sortByContinentDTO(){
        List<TripDTO> sortedByContinentTripsDTO = new LinkedList<>();
        List<Trip> sortedByContinentList = tripDAO.sortTripsByContinent();
        for(Trip t: sortedByContinentList){
            TripDTO tripDTO = tripToTripDTO(t);
            sortedByContinentTripsDTO.add(tripDTO);
        }
        return sortedByContinentTripsDTO;
    }

    public List<TripDTO> sortByPromoted(){
        List<TripDTO> listOfPromotedTripsDTO = new LinkedList<>();
        List<Trip> promotedTripList = tripDAO.findPromotedTrips();
        for(Trip t: promotedTripList){
            TripDTO tripDTO = tripToTripDTO(t);
            listOfPromotedTripsDTO.add(tripDTO);
        }
    return listOfPromotedTripsDTO;
    }

    public List<TripDTO> getListOfUpcomingTrips(){
        List<Trip> listOfUpcomingTrips = tripDAO.findListOfUpcomingTrips();
        List<TripDTO> listOfUpcomingTripsDTO = new LinkedList<>();
        if(listOfUpcomingTrips.size()>0) {
            for (Trip t : listOfUpcomingTrips) {
                TripDTO tripDTO = tripToTripDTO(t);
                listOfUpcomingTripsDTO.add(tripDTO);
            }
        }else{
            System.out.println("there are no upcoming trips");
        }
    return listOfUpcomingTripsDTO;
    }

    public List<TripDTO> getListOfTripsFromCity(String departureCity){
        List<Trip> listOfTripsFromCity = tripDAO.findTripsByDepartureCity(departureCity);
        List<TripDTO> listOfTripsFromCityDTO = new LinkedList<>();
        for(Trip t: listOfTripsFromCity){
            TripDTO tripDTO = tripToTripDTO(t);
            listOfTripsFromCityDTO.add(tripDTO);
        }
        return listOfTripsFromCityDTO;
    }

    public List<TripDTO> getTripsWithDepartureDateBetweenDates(SearchFromDateToDate searchFromDateToDate){
        List<Trip> listOfTripsDateBetween = tripDAO.getTripsWhereDepartureDateBetweenDates(searchFromDateToDate.getFromDate(), searchFromDateToDate.getToDate());
        List<TripDTO> listOfTripsDepDateBetweenDTO = new LinkedList<>();
        for(Trip t:listOfTripsDateBetween){
            TripDTO tripDTO = tripToTripDTO(t);
            listOfTripsDepDateBetweenDTO.add(tripDTO);
        }
        return listOfTripsDepDateBetweenDTO;
    }

    public List<TripDTO> getTripsOfNumberOfNights(long numberOfNights){
        List<Trip> tripsWithNumberOfNights = tripDAO.findTripsWithNumberOfNights(numberOfNights);
        List<TripDTO> tripWithNumberOfNightsDTO = new LinkedList<>();
        for(Trip t:tripsWithNumberOfNights){
            TripDTO tripDTO = tripToTripDTO(t);
            tripWithNumberOfNightsDTO.add(tripDTO);
        }
        return tripWithNumberOfNightsDTO;
    }

    public TripDTO tripToTripDTO(Trip trip){
        TripDTO tripDTO = new TripDTO();

        Flight flight = trip.getDepartureFlight();
        FlightDTO flightDTO = new FlightDTO();
        flightDTO.setFlightNumber(flight.getFlightNumber());
        flightDTO.setDepartureDate(flight.getDepartureDate());
        flightDTO.setTotalNumOfSeats(flight.getTotalNumOfSeats());
        flightDTO.setAvailableNumOfSeats(flight.getAvailableNumOfSeats());
        flightDTO.setPrice(flight.getFlightPrice());

        Airport airport = flight.getAirport();
        AirportDTO airportDTO = new AirportDTO();
        airportDTO.setName(airport.getName());
        City city = airport.getCity();
        CityDTO cityDTO = new CityDTO();
        cityDTO.setName(city.getName());
        Country country = city.getCountry();
        CountryDTO countryDTO = new CountryDTO();
        countryDTO.setName(country.getName());
        Continent continent = country.getContinent();
        ContinentDTO continentDTO = new ContinentDTO();
        continentDTO.setName(continent.getName());
        countryDTO.setContinentDTO(continentDTO);
        cityDTO.setCountryDTO(countryDTO);
        airportDTO.setCityDTO(cityDTO);
        flightDTO.setAirportDTO(airportDTO);

        Airport returnAirport = flight.getReturnAirport();
        AirportDTO returnAirportDTO = new AirportDTO();
        returnAirportDTO.setName(returnAirport.getName());
        City returnCity = returnAirport.getCity();
        CityDTO returnCityDTO = new CityDTO();
        returnCityDTO.setName(returnCity.getName());
        Country returnCountry = returnCity.getCountry();
        CountryDTO returnCountryDto = new CountryDTO();
        returnCountryDto.setName(returnCountry.getName());
        Continent returnContinent = returnCountry.getContinent();
        ContinentDTO returnContinentDto = new ContinentDTO();
        returnContinentDto.setName(returnContinent.getName());
        returnCountryDto.setContinentDTO(returnContinentDto);
        returnCityDTO.setCountryDTO(returnCountryDto);
        returnAirportDTO.setCityDTO(returnCityDTO);
        flightDTO.setReturnAirportDTO(returnAirportDTO);

        tripDTO.setDepartureFlightDTO(flightDTO);

        Flight returnFlight = trip.getReturnFlight();
        FlightDTO returnFlightDTO = new FlightDTO();

        returnFlightDTO.setFlightNumber(returnFlight.getFlightNumber());
        returnFlightDTO.setDepartureDate(returnFlight.getDepartureDate());
        returnFlightDTO.setTotalNumOfSeats(returnFlight.getTotalNumOfSeats());
        returnFlightDTO.setAvailableNumOfSeats(returnFlight.getAvailableNumOfSeats());
        returnFlightDTO.setPrice(returnFlight.getFlightPrice());

        Airport airportBack = returnFlight.getAirport();
        AirportDTO airportBackDTO = new AirportDTO();
        airportBackDTO.setName(airportBack.getName());
        City cityBack = airportBack.getCity();
        CityDTO cityBackDTO = new CityDTO();
        cityBackDTO.setName(cityBack.getName());
        Country countryBack = cityBack.getCountry();
        CountryDTO countryBackDTO = new CountryDTO();
        countryBackDTO.setName(countryBack.getName());
        Continent contBack = countryBack.getContinent();
        ContinentDTO contBackDTO = new ContinentDTO();
        contBackDTO.setName(contBack.getName());
        countryBackDTO.setContinentDTO(contBackDTO);
        cityBackDTO.setCountryDTO(countryBackDTO);
        airportBackDTO.setCityDTO(cityBackDTO);
        returnFlightDTO.setAirportDTO(airportBackDTO);

        Airport homeAirport = returnFlight.getReturnAirport();
        AirportDTO homeAirportDTO = new AirportDTO();
        homeAirportDTO.setName(homeAirport.getName());
        City homeCity = homeAirport.getCity();
        CityDTO homeCityDTO = new CityDTO();
        homeCityDTO.setName(homeCity.getName());
        Country homeCountry = homeCity.getCountry();
        CountryDTO homeCountryDTO = new CountryDTO();
        homeCountryDTO.setName(homeCountry.getName());
        Continent homeContinent = homeCountry.getContinent();
        ContinentDTO homeContinentDTO = new ContinentDTO();
        homeContinentDTO.setName(homeContinent.getName());
        homeCountryDTO.setContinentDTO(homeContinentDTO);
        homeCityDTO.setCountryDTO(homeCountryDTO);
        homeAirportDTO.setCityDTO(homeCityDTO);
        returnFlightDTO.setReturnAirportDTO(homeAirportDTO);

        tripDTO.setReturnFlightDTO(returnFlightDTO);

        Hotel hotel = trip.getHotel();
        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setName(hotel.getName());
        hotelDTO.setHotelDescription(hotel.getHotelDescription());
        hotelDTO.setStandard(hotel.getStandard());
            City hotelCity = hotel.getCity();
            CityDTO hotelCityDTO = new CityDTO();
            hotelCityDTO.setName(hotelCity.getName());
                Country hotelCountry = hotelCity.getCountry();
                CountryDTO hotelCountryDTO = new CountryDTO();
                hotelCountryDTO.setName(hotelCountry.getName());
                    Continent hotelContinent = hotelCountry.getContinent();
                    ContinentDTO hotelContinentDTO = new ContinentDTO();
                    hotelContinentDTO.setName(hotelContinent.getName());
        hotelCountryDTO.setContinentDTO(hotelContinentDTO);
        hotelCityDTO.setCountryDTO(hotelCountryDTO);
        hotelDTO.setCityDTO(hotelCityDTO);

        tripDTO.setHotelDTO(hotelDTO);
        tripDTO.setPromoted(trip.isPromoted());
    return tripDTO;
    }

    public List<TripDTO> findAllTrips(){
        List<Trip> listOfAllTrips = tripDAO.findAllTrips();
        List<TripDTO> listOfAllTripDTO = new LinkedList<>();

        for(Trip t: listOfAllTrips){
            TripDTO tripDTO = new TripDTO();
            tripDTO = tripToTripDTO(t);
            listOfAllTripDTO.add(tripDTO);

        }
    return listOfAllTripDTO;
    }

    public Trip tripDTOToTrip(TripDTO tripDTO){
        Trip trip = new Trip();

        FlightDTO flightDTO = tripDTO.getDepartureFlightDTO();
        Flight flight = new Flight();
        flight.setFlightNumber(flightDTO.getFlightNumber());
        flight.setDepartureDate(flightDTO.getDepartureDate());
        flight.setTotalNumOfSeats(flightDTO.getTotalNumOfSeats());
        flight.setAvailableNumOfSeats(flightDTO.getAvailableNumOfSeats());
        flight.setFlightPrice(flightDTO.getPrice());

        AirportDTO airportDTO = flightDTO.getAirportDTO();
        Airport airport = new Airport();
        airport.setName(airportDTO.getName());
        CityDTO cityDTO = airportDTO.getCityDTO();
        City city = new City();
        city.setName(cityDTO.getName());
        CountryDTO countryDTO = cityDTO.getCountryDTO();
        Country country = new Country();
        country.setName(countryDTO.getName());
        ContinentDTO continentDTO = countryDTO.getContinentDTO();
        Continent continent = new Continent();
        continent.setName(continentDTO.getName());
        country.setContinent(continent);
        city.setCountry(country);
        airport.setCity(city);
        flight.setAirport(airport);

        AirportDTO destAirportDTO = flightDTO.getReturnAirportDTO();
        Airport destAirport = new Airport();
        destAirport.setName(destAirportDTO.getName());
        CityDTO destCityDTO = destAirportDTO.getCityDTO();
        City destCity = new City();
        destCity.setName(destCityDTO.getName());
        CountryDTO destCountryDTO = destCityDTO.getCountryDTO();
        Country destCountry = new Country();
        destCountry.setName(destCountryDTO.getName());
        ContinentDTO destContinentDTO = countryDTO.getContinentDTO();
        Continent destContinent = new Continent();
        destContinent.setName(destContinentDTO.getName());
        destCountry.setContinent(destContinent);
        destCity.setCountry(destCountry);
        destAirport.setCity(destCity);
        flight.setReturnAirport(destAirport);

        trip.setDepartureFlight(flight);

        FlightDTO returnFlightDTO = tripDTO.getReturnFlightDTO();
        Flight returnFlight = new Flight();
        returnFlight.setFlightNumber(returnFlightDTO.getFlightNumber());
        returnFlight.setDepartureDate(returnFlightDTO.getDepartureDate());
        returnFlight.setTotalNumOfSeats(returnFlightDTO.getTotalNumOfSeats());
        returnFlight.setAvailableNumOfSeats(returnFlightDTO.getAvailableNumOfSeats());
        returnFlight.setFlightPrice(returnFlightDTO.getPrice());

        AirportDTO fromAirportDTO = returnFlightDTO.getAirportDTO();
        Airport fromAirport = new Airport();
        fromAirport.setName(fromAirportDTO.getName());
        CityDTO fromCityDTO = fromAirportDTO.getCityDTO();
        City fromCity = new City();
        fromCity.setName(fromCityDTO.getName());
        CountryDTO fromCountryDTO = fromCityDTO.getCountryDTO();
        Country fromCountry = new Country();
        fromCountry.setName(fromCountryDTO.getName());
        ContinentDTO fromContinentDTO = fromCountryDTO.getContinentDTO();
        Continent fromContinent = new Continent();
        fromContinent.setName(fromContinentDTO.getName());
        fromCountry.setContinent(fromContinent);
        fromCity.setCountry(fromCountry);
        fromAirport.setCity(fromCity);
        returnFlight.setAirport(fromAirport);

        AirportDTO toAirportDTO = returnFlightDTO.getReturnAirportDTO();
        Airport toAirport = new Airport();
        toAirport.setName(toAirportDTO.getName());
        CityDTO toCityDTO = toAirportDTO.getCityDTO();
        City toCity = new City();
        toCity.setName(toCityDTO.getName());
        CountryDTO toCountryDTO = toCityDTO.getCountryDTO();
        Country toCountry = new Country();
        toCountry.setName(toCountryDTO.getName());
        ContinentDTO toContinentDTO = toCountryDTO.getContinentDTO();
        Continent toContinent = new Continent();
        toContinent.setName(toContinentDTO.getName());
        toCountry.setContinent(toContinent);
        toCity.setCountry(toCountry);
        toAirport.setCity(toCity);
        returnFlight.setReturnAirport(toAirport);

        trip.setReturnFlight(returnFlight);

        HotelDTO hotelDTO = tripDTO.getHotelDTO();
        Hotel hotel = new Hotel();
        hotel.setName(hotelDTO.getName());
        hotel.setHotelDescription(hotelDTO.getHotelDescription());
        hotel.setStandard(hotelDTO.getStandard());
        CityDTO hotelCityDTO = hotelDTO.getCityDTO();
        City hotelCity = new City();
        hotelCity.setName(hotelCityDTO.getName());
        CountryDTO hotelCountryDTO = hotelCityDTO.getCountryDTO();
        Country hotelCountry = new Country();
        hotelCountry.setName(hotelCountryDTO.getName());
        ContinentDTO hotelContinentDTO = hotelCountryDTO.getContinentDTO();
        Continent hotelContinent = new Continent();
        hotelContinent.setName(hotelContinentDTO.getName());
        hotelCountry.setContinent(hotelContinent);
        hotelCity.setCountry(hotelCountry);
        hotel.setCity(hotelCity);

        trip.setHotel(hotel);

        trip.setPromoted(tripDTO.isPromoted());
    return trip;
    }
}