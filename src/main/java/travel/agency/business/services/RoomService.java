package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.RoomDTO;
import travel.agency.persistance.DAO.RoomDAO;
import travel.agency.persistance.entities.Hotel;
import travel.agency.persistance.entities.Room;

@Service
public class RoomService {
    @Autowired
    RoomDAO roomDAO;
    @Autowired
    HotelService hotelService;

    public void addRooms(RoomDTO roomDTO){
        Room room = new Room();
        room.setFromDate(roomDTO.getFromDate());
        room.setToDate(roomDTO.getToDate());
        room.setDoubleRoomsAvailable(roomDTO.getDoubleRoomsAvailable());
        room.setPriceForDouble(roomDTO.getPriceForDouble());
        room.setSingleRoomsAvailable(roomDTO.getSingleRoomsAvailable());
        room.setPriceForSingle(roomDTO.getPriceForSingle());
        room.setExtraBedsAvailable(roomDTO.getExtraBedsAvailable());
        room.setPriceForExtra(roomDTO.getPriceForExtra());
        Hotel hotel = hotelService.findHotelByName(roomDTO.getHotelDTO());
        room.setHotel(hotel);
        roomDAO.addRoom(room);
    }


}
