package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.*;
import travel.agency.business.DTO.searchDTO.SearchFlightDTO;
import travel.agency.persistance.DAO.FlightDAO;
import travel.agency.persistance.entities.Airport;
import travel.agency.persistance.entities.City;
import travel.agency.persistance.entities.Country;
import travel.agency.persistance.entities.Flight;

import java.util.LinkedList;
import java.util.List;

@Service
public class FlightService {
    @Autowired
    FlightDAO flightDAO;
    @Autowired
    AirportService airportService;
    @Autowired
    CityService cityService;

    public void addFlight(FlightDTO flightDTO) {
        Flight flight = new Flight();
        flight.setFlightNumber(flightDTO.getFlightNumber());
        flight.setDepartureDate(flightDTO.getDepartureDate());
        flight.setTotalNumOfSeats(flightDTO.getTotalNumOfSeats());
        flight.setAvailableNumOfSeats(flightDTO.getAvailableNumOfSeats());
        flight.setFlightPrice(flightDTO.getPrice());

        AirportDTO airportDTO = flightDTO.getAirportDTO();
        AirportDTO returnAirportDTO = flightDTO.getReturnAirportDTO();

        Airport airport = airportService.findAirportByName(airportDTO);
        flight.setAirport(airport);

        Airport returnAirport = airportService.findAirportByName(returnAirportDTO);
        flight.setReturnAirport(returnAirport);
        flightDAO.insertFlight(flight);
    }

    public Flight findFlightByNumber(String number) {
        Flight flight = flightDAO.findFlightByNumber(number);
    return flight;
    }

    public FlightDTO findFlightDTOtByNumber(String flightNumber) {
        Flight flight = flightDAO.findFlightByNumber(flightNumber);
        FlightDTO flightDTO = new FlightDTO();
        if(flight !=null) {
            flightDTO.setFlightNumber(flight.getFlightNumber());
            flightDTO.setAvailableNumOfSeats(flight.getAvailableNumOfSeats());
            flightDTO.setDepartureDate(flight.getDepartureDate());
            flightDTO.setTotalNumOfSeats(flight.getTotalNumOfSeats());
            flightDTO.setPrice(flight.getFlightPrice());

            Airport airport = flight.getAirport();
            AirportDTO airportDTO = airportService.findAirportDTO(airport.getName());

            flightDTO.setAirportDTO(airportDTO);

            Airport returnAirport = flight.getReturnAirport();
            AirportDTO returnAirportDTO = new AirportDTO();
            returnAirportDTO.setName(returnAirport.getName());
            flightDTO.setReturnAirportDTO(returnAirportDTO);
        }else {
            System.out.println("The flight number is not valid");
        }
        return flightDTO;
    }

    public List<FlightDTO> findFlightsFromCityToCity(SearchFlightDTO searchFlightDTO){
        List<FlightDTO> listOfFlightDTOSFromCityToCity = new LinkedList<>();
        CityDTO cityDTO = searchFlightDTO.getDepartureCity();
        City city = new City();
        city.setName(cityDTO.getName());

        CityDTO returnCityDTO = searchFlightDTO.getReturnCity();
        City returnCity = new City();
        returnCity.setName(returnCityDTO.getName());

        List<Flight> listOfFlightsFromCityToCity = flightDAO.findFlightsFromCityToCity(city, returnCity);

        for(Flight f:listOfFlightsFromCityToCity){
            FlightDTO flightDTO = new FlightDTO();

            flightDTO.setFlightNumber(f.getFlightNumber());
            flightDTO.setDepartureDate(f.getDepartureDate());
            flightDTO.setAvailableNumOfSeats(f.getAvailableNumOfSeats());
            flightDTO.setTotalNumOfSeats(f.getTotalNumOfSeats());
            flightDTO.setPrice(f.getFlightPrice());

            Airport airport = f.getAirport();
            AirportDTO airportDTO = new AirportDTO();
            airportDTO.setName(airport.getName());
            airportDTO.setCityDTO(cityDTO);
            flightDTO.setAirportDTO(airportDTO);

            Airport returnAirport = f.getReturnAirport();
            AirportDTO returnAirportDTO = new AirportDTO();
            returnAirportDTO.setName(returnAirport.getName());
            returnAirportDTO.setCityDTO(returnCityDTO);
            flightDTO.setReturnAirportDTO(returnAirportDTO);

            listOfFlightDTOSFromCityToCity.add(flightDTO);
        }
        return listOfFlightDTOSFromCityToCity;
    }

    public List<FlightDTO> findFlightFromCity(CityDTO cityDTO){
        List<FlightDTO> flightDTOS = new LinkedList<>();
        City city = new City();
        city.setName(cityDTO.getName());

        List<Flight> flights = flightDAO.findFlightInCity(city);
        for(Flight flight: flights) {
            FlightDTO flightDTO = new FlightDTO();

            Airport airport = flight.getAirport();
            AirportDTO airportDTO = new AirportDTO();
            airportDTO.setName(airport.getName());
            airportDTO.setCityDTO(cityDTO);
            flightDTO.setAirportDTO(airportDTO);

            Airport returnAirport = flight.getReturnAirport();
            AirportDTO returnAirportDTO = new AirportDTO();
            returnAirportDTO.setName(returnAirport.getName());
            City returnCity = flight.getAirport().getCity();
            CityDTO returnCityDTO = new CityDTO();
            returnCityDTO.setName(returnCity.getName());
            returnAirportDTO.setCityDTO(returnCityDTO);
            flightDTO.setReturnAirportDTO(returnAirportDTO);

            flightDTO.setAvailableNumOfSeats(flight.getAvailableNumOfSeats());
            flightDTO.setTotalNumOfSeats(flight.getTotalNumOfSeats());
            flightDTO.setDepartureDate(flight.getDepartureDate());
            flightDTO.setFlightNumber(flight.getFlightNumber());
            flightDTO.setPrice(flight.getFlightPrice());
            flightDTOS.add(flightDTO);
        }
        return flightDTOS;
    }

    public List<FlightDTO> findFlightsFromCityToCityAndDate(SearchFlightDTO searchFlightDTO){
        List<FlightDTO> flightDTOSFromCityToCityAndDate = new LinkedList<>();
        Long numOfFlightsByDate = flightDAO.findFlightByDate(searchFlightDTO.getDepartureDate());
        if(numOfFlightsByDate !=null){
        List<Flight> flightsFromCityToCityAndDate = flightDAO.findFlightFromCityToCity(searchFlightDTO.getDepartureCity().getName(), searchFlightDTO.getReturnCity().getName(), searchFlightDTO.getDepartureDate());
           for(Flight f: flightsFromCityToCityAndDate){
               FlightDTO flightDTO = new FlightDTO();
               flightDTO.setFlightNumber(f.getFlightNumber());
               flightDTO.setDepartureDate(f.getDepartureDate());
               flightDTO.setPrice(f.getFlightPrice());
               flightDTO.setTotalNumOfSeats(f.getTotalNumOfSeats());
               flightDTO.setAvailableNumOfSeats(f.getAvailableNumOfSeats());

                Airport airport = f.getAirport();
                AirportDTO airportDTO = new AirportDTO();
                airportDTO.setName(airport.getName());
                    City city = f.getAirport().getCity();
                    CityDTO cityDTO =new CityDTO();
                    cityDTO.setName(city.getName());
                        Country country = city.getCountry();
                        CountryDTO countryDTO = new CountryDTO();
                        countryDTO.setName(country.getName());
                    cityDTO.setCountryDTO(countryDTO);
                 airportDTO.setCityDTO(cityDTO);
                flightDTO.setAirportDTO(airportDTO);

                 Airport returnAirport = f.getReturnAirport();
                 AirportDTO returnAirportDTO = new AirportDTO();
                 returnAirportDTO.setName(returnAirport.getName());
                    City returnCity = f.getReturnAirport().getCity();
                    CityDTO returnCityDTO = new CityDTO();
                    returnCityDTO.setName(returnCity.getName());
                        Country returnCountry = returnCity.getCountry();
                        CountryDTO returnCountryDTO = new CountryDTO();
                        returnCountryDTO.setName(returnCountry.getName());
                    returnCityDTO.setCountryDTO(returnCountryDTO);
                  returnAirportDTO.setCityDTO(returnCityDTO);
                 flightDTO.setReturnAirportDTO(returnAirportDTO);
           flightDTOSFromCityToCityAndDate.add(flightDTO);
        }
           }else{
                System.out.println("There are no flights for that date");
           }

    return flightDTOSFromCityToCityAndDate;
    }


}
