package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.*;
import travel.agency.persistance.DAO.HotelDAO;
import travel.agency.persistance.entities.*;

import java.util.LinkedList;
import java.util.List;

@Service
public class HotelService {

    @Autowired
    HotelDAO hotelDAO;
    @Autowired
    CityService cityService;

    public void addHotel(HotelDTO hotelDTO){
        Hotel hotel = new Hotel();
        hotel.setName(hotelDTO.getName());
        hotel.setHotelDescription(hotelDTO.getHotelDescription());
        hotel.setStandard(hotelDTO.getStandard());
        City city = cityService.findCityByName(hotelDTO.getCityDTO());
        hotel.setCity(city);
        hotelDAO.insertHotel(hotel);
    }

    public Hotel findHotelByName(HotelDTO hotelDTO){
        Hotel hotel = hotelDAO.findHotelByName(hotelDTO.getName(), hotelDTO.getCityDTO().getName());
        return hotel;
    }

    public HotelDTO findHotelByStringName(String name, String cityName){
        Hotel hotel = hotelDAO.findHotelByName(name, cityName);
        HotelDTO hotelDTO = new HotelDTO();
        if(hotel != null){
           hotelDTO.setName(hotel.getName());
           hotelDTO.setHotelDescription(hotel.getHotelDescription());
           hotelDTO.setStandard(hotel.getStandard());
           CityDTO cityDTO = cityService.findCityDTOByName(hotel.getCity().getName());
           hotelDTO.setCityDTO(cityDTO);
        }
        return hotelDTO;
    }

    public List<HotelDTO> findListOfAllHotels(){
        List<Hotel> listOfAllHotels = hotelDAO.findAllHotels();
        List<HotelDTO> listOfAllHotelsDTO = new LinkedList<>();

        for(Hotel h: listOfAllHotels){
            HotelDTO hotelDTO = new HotelDTO();
            hotelDTO.setName(h.getName());
            hotelDTO.setHotelDescription(h.getHotelDescription());
            hotelDTO.setStandard(h.getStandard());
            City city = h.getCity();
            CityDTO cityDTO = new CityDTO();
            cityDTO.setName(city.getName());
            Country country = city.getCountry();
            CountryDTO countryDTO = new CountryDTO();
            countryDTO.setName(country.getName());
            Continent continent = country.getContinent();
            ContinentDTO continentDTO = new ContinentDTO();
            continentDTO.setName(continent.getName());
            countryDTO.setContinentDTO(continentDTO);
            cityDTO.setCountryDTO(countryDTO);
            hotelDTO.setCityDTO(cityDTO);

            listOfAllHotelsDTO.add(hotelDTO);
        }
     return listOfAllHotelsDTO;
    }

    public List<RoomDTO> findRoomsDTOInHotel(HotelDTO hotelDTO) {
        Hotel hotel = hotelDAO.findHotelByName(hotelDTO.getName(), hotelDTO.getCityDTO().getName());
        List<RoomDTO> roomDTOS = new LinkedList<>();
        if (hotel != null) {
            List<Room> rooms = hotel.getRooms();
            for (Room r : rooms) {
                RoomDTO roomDTO = new RoomDTO();
                roomDTO.setHotelDTO(hotelDTO);
                roomDTO.setDoubleRoomsAvailable(r.getDoubleRoomsAvailable());
                roomDTO.setPriceForDouble(r.getPriceForDouble());
                roomDTO.setSingleRoomsAvailable(r.getSingleRoomsAvailable());
                roomDTO.setPriceForSingle(r.getPriceForSingle());
                roomDTO.setExtraBedsAvailable(r.getExtraBedsAvailable());
                roomDTO.setPriceForExtra(r.getPriceForExtra());
                roomDTO.setFromDate(r.getFromDate());
                roomDTO.setToDate(r.getToDate());
                roomDTOS.add(roomDTO);
            }
        }else{
            System.out.println("No such hotel");
        }
    return roomDTOS;
    }
}
