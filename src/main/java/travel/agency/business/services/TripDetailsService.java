package travel.agency.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import travel.agency.business.DTO.*;
import travel.agency.business.DTO.searchDTO.UserAndSearchDetailsDTO;
import travel.agency.persistance.DAO.*;
import travel.agency.persistance.entities.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;


@Service
public class TripDetailsService {
    @Autowired
    private TripDetailsDAO tripDetailsDAO;

    @Autowired
    private FlightDAO flightDAO;
    @Autowired
    private RoomDAO roomDAO;
    @Autowired
    private  TripService tripService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private TripDAO tripDAO;


    public String buyTrip( UserAndSearchDetailsDTO searchTripDetailsDTO){
        TripDetails tripDetails = new TripDetails();
        String message = new String();
        User user = new User();
        if(userService.validateCredentialsOne(searchTripDetailsDTO.getUserDTO().getUserName(), searchTripDetailsDTO.getUserDTO().getPassword())){
            user.setUserName(searchTripDetailsDTO.getUserDTO().getUserName());
            user.setId(userDAO.getUserId(searchTripDetailsDTO.getUserDTO().getUserName() ));
        }else {
           userService.addUser(searchTripDetailsDTO.getUserDTO());
           user.setUserName(searchTripDetailsDTO.getUserDTO().getUserName());
           user.setId(userDAO.getUserId(searchTripDetailsDTO.getUserDTO().getUserName()));
        }
        tripDetails.setUser(user);

        Trip boughtTrip = new Trip();
        TripDTO boughtTripDTO = tripService.findTripToCityFromDateToDateAndNumOfPersons(searchTripDetailsDTO.getSearchTripDetailsDTO());
        if(boughtTripDTO != null){
            FlightDTO flightDTO = boughtTripDTO.getDepartureFlightDTO();
            Flight flight = new Flight();

            flight.setFlightNumber(flightDTO.getFlightNumber());
            flight.setFlightPrice(flightDTO.getPrice());
            flight.setDepartureDate(flightDTO.getDepartureDate());
            flight.setTotalNumOfSeats(flightDTO.getTotalNumOfSeats());
            flight.setAvailableNumOfSeats(flightDTO.getAvailableNumOfSeats());

            Airport airport = new Airport();
            AirportDTO airportDTO = flightDTO.getAirportDTO();
            airport.setName(airportDTO.getName());
                City city = new City();
                CityDTO cityDTO = airportDTO.getCityDTO();
                city.setName(cityDTO.getName());
                Country country = new Country();
                CountryDTO countryDTO = cityDTO.getCountryDTO();
                country.setName(countryDTO.getName());
                city.setCountry(country);
            airport.setCity(city);
            flight.setAirport(airport);

            AirportDTO returnAirportDTO = flightDTO.getReturnAirportDTO();
            Airport returnAirport = new Airport();
            returnAirport.setName(returnAirportDTO.getName());
                City returnCity = new City();
                CityDTO returnCityDTO = returnAirportDTO.getCityDTO();
                returnCity.setName(returnCityDTO.getName());
                Country returnCountry = new Country();
                CountryDTO returnCountryDTO = cityDTO.getCountryDTO();
                returnCountry.setName(returnCountryDTO.getName());
                returnCity.setCountry(returnCountry);
                returnAirport.setCity(returnCity);
                flight.setReturnAirport(returnAirport);

            boughtTrip.setDepartureFlight(flight);

            int initialNumOfAvailableSeats = flight.getAvailableNumOfSeats();
            System.out.println("numOfAvailableSeats+" + initialNumOfAvailableSeats);

            int numOfSeatsBought = Math.toIntExact(searchTripDetailsDTO.getSearchTripDetailsDTO().getNumberOfPersons());
            System.out.println("numOfSeatsBought+"+numOfSeatsBought);

            int remainedSeats = initialNumOfAvailableSeats-numOfSeatsBought;
            System.out.println("departureDate+" + flight.getDepartureDate()+flight.getAirport().getCity().getName()+" "+flight.getReturnAirport().getCity().getName());
            Flight updatedFlight = flightDAO.findFlightByParameters(flight.getDepartureDate(), flight.getAirport().getCity().getName(),flight.getReturnAirport().getCity().getName());
            updatedFlight.setAvailableNumOfSeats(remainedSeats);
            try {
                flightDAO.updateFlight(updatedFlight);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Flight returnFlight = new Flight();
            FlightDTO returnFlightDTO = boughtTripDTO.getReturnFlightDTO();

            returnFlight.setFlightNumber(returnFlightDTO.getFlightNumber());
            returnFlight.setFlightPrice(returnFlightDTO.getPrice());
            returnFlight.setTotalNumOfSeats(returnFlightDTO.getTotalNumOfSeats());
            returnFlight.setAvailableNumOfSeats(returnFlightDTO.getAvailableNumOfSeats());
            returnFlight.setDepartureDate(returnFlightDTO.getDepartureDate());

            Airport returnFlightAirport = new Airport();
            AirportDTO returnFlightAirportDTO = returnFlightDTO.getAirportDTO();
            returnFlightAirport.setName(returnFlightAirportDTO.getName());
            City returnFlightCity = new City();
            CityDTO returnFlightCityDTO = returnFlightAirportDTO.getCityDTO();
            returnFlightCity.setName(returnFlightCityDTO.getName());
            Country fromCountry = new Country();
            CountryDTO fromCountryDTO = returnFlightCityDTO.getCountryDTO();
            fromCountry.setName(fromCountryDTO.getName());
            returnFlightCity.setCountry(fromCountry);
            returnFlightAirport.setCity(returnFlightCity);
            returnFlight.setAirport(returnFlightAirport);

            Airport homeAirport = new Airport();
            AirportDTO homeAirportDTO = returnFlightDTO.getReturnAirportDTO();
            homeAirport.setName(homeAirportDTO.getName());
            City homeCity = new City();
            CityDTO homeCityDTO = airportDTO.getCityDTO();
            homeCity.setName(homeCityDTO.getName());
            Country homeCountry = country;
            homeCity.setCountry(country);
            homeAirport.setCity(homeCity);
            returnFlight.setReturnAirport(homeAirport);

            boughtTrip.setReturnFlight(returnFlight);

            int numOfReturnSeatsAvailable = returnFlight.getAvailableNumOfSeats();
            int remainedReturnSeatsAvailable = numOfReturnSeatsAvailable - numOfSeatsBought;

            Flight updatedReturnFlight = flightDAO.findFlightByParameters(returnFlight.getDepartureDate(),
                                                                          returnFlight.getAirport().getCity().getName(),
                                                                          returnFlight.getReturnAirport().getCity().getName());
            updatedReturnFlight.setAvailableNumOfSeats(remainedReturnSeatsAvailable);
            try {
                flightDAO.updateFlight(updatedReturnFlight);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Hotel hotel = new Hotel();
            HotelDTO hotelDTO = boughtTripDTO.getHotelDTO();
            hotel.setName(hotelDTO.getName());
            City hotelCity = new City();
            CityDTO hotelCityDTO = hotelDTO.getCityDTO();
            hotelCity.setName(hotelCityDTO.getName());
            hotel.setCity(homeCity);
            hotel.setHotelDescription(hotelDTO.getHotelDescription());
            hotel.setStandard(hotelDTO.getStandard());

            boughtTrip.setHotel(hotel);

            boughtTrip.setPromoted(boughtTripDTO.isPromoted());
            boughtTrip.setId(tripDAO.getTripId(searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                                                searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureCity().getName(),
                                                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate(),
                                                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                                                searchTripDetailsDTO.getSearchTripDetailsDTO().getHotel().getName()));
        tripDetails.setTrip(boughtTrip);
        }else{
            message ="sorry, we have no trips available for your search";
        }

        RandomTripDetailsNumber randomTripDetailsNumber = new RandomTripDetailsNumber();
        String tripDetailsNumber = randomTripDetailsNumber.getRandomNumber();
        tripDetails.setNumber(tripDetailsNumber);

        Long numberOfPersonsInTrip = searchTripDetailsDTO.getSearchTripDetailsDTO().getNumberOfPersons();
        Long numberOfDoubleRooms = null;
        Long numberOfSingleRooms = null;
        if(numberOfPersonsInTrip%2 == 0){
            numberOfDoubleRooms = numberOfPersonsInTrip/2;
            System.out.println("numOfDoubleRooms+"+numberOfDoubleRooms);
        }else{
            numberOfDoubleRooms = (numberOfPersonsInTrip -1)/2;
            numberOfSingleRooms = numberOfPersonsInTrip-numberOfDoubleRooms*2;
            System.out.println("numOfDounble+ "+numberOfDoubleRooms+"numOfSingle+"+numberOfSingleRooms);
        }
        tripDetails.setDoubleRoomsBooked(numberOfDoubleRooms);

        Long initialNumOfDoubleRooms = roomDAO.countNumberOfDoubleRoomsAvailableForHotelAndPeriod(searchTripDetailsDTO.getSearchTripDetailsDTO().getHotel().getName(),
                                                                                                  searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                                                                                                  searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                                                                                                  searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate());
        Long doubleRoomsRemained = initialNumOfDoubleRooms-numberOfDoubleRooms;

        tripDetails.setSingleRoomsBooked(numberOfSingleRooms);
        Long initialNumOfSingleRooms = roomDAO.countNumberOfSingleRoomsAvailableForHotelAndPeriod(searchTripDetailsDTO.getSearchTripDetailsDTO().getHotel().getName(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate());

        Long singleRoomsRemained = initialNumOfSingleRooms - numberOfSingleRooms;
        Room updatedRoom = roomDAO.findRoomByParameters(searchTripDetailsDTO.getSearchTripDetailsDTO().getHotel().getName(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate());

        updatedRoom.setDoubleRoomsAvailable(doubleRoomsRemained);
        updatedRoom.setSingleRoomsAvailable(singleRoomsRemained);
        try {
            roomDAO.updateRooms(updatedRoom);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Long priceForDouble = roomDAO.getPriceForDoubleRoomToHotelAndPeriod(searchTripDetailsDTO.getSearchTripDetailsDTO().getHotel().getName(),
                                                                            searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                                                                            searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                                                                            searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate());
        //System.out.println("priceForDouble+"+priceForDouble);
        Long priceForSingle = roomDAO.getPriceForSingleRoomToHotelAndPeriod(searchTripDetailsDTO.getSearchTripDetailsDTO().getHotel().getName(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate());

        //System.out.println("priceForSingle+"+priceForSingle);
        Long priceForDepartureFlight = flightDAO.getPriceForFlight(searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureCity().getName(),
                                                                    searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureDate(),
                                                                    searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName());
        //System.out.println("priceForFlight+"+priceForDepartureFlight);

        Long priceForReturnFlight = flightDAO.getPriceForFlight(searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnCity().getName(),
                                                                searchTripDetailsDTO.getSearchTripDetailsDTO().getReturnDate(),
                                                                searchTripDetailsDTO.getSearchTripDetailsDTO().getDepartureCity().getName());
        //System.out.println("priceReturnFlight+"+ priceForReturnFlight);
        Long amountSpent = priceForDouble*numberOfDoubleRooms+priceForSingle*numberOfSingleRooms+priceForDepartureFlight+priceForReturnFlight;
        System.out.println("amountSpent+"+amountSpent);
        tripDetails.setAmountSpent(amountSpent);
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        tripDetails.setPurchaseDate(date);
        tripDetailsDAO.addTripDetails(tripDetails);
        message = "Trip successfully bought! Total price "+amountSpent;
        return message;
    }

    public List<TripDetailsDTO> findListOfRecentTrips(String userName){
        List<TripDetails> listOfRecentTrips = tripDetailsDAO.findListOfRecentTrips(userName);
        List<TripDetailsDTO> listOfRecentTripsDTO = new LinkedList<>();
        for(TripDetails t:listOfRecentTrips){
            TripDetailsDTO tripDetailsDTO = new TripDetailsDTO();
            tripDetailsDTO.setNumber(t.getNumber());
            tripDetailsDTO.setAmountSpent(t.getAmountSpent());
            tripDetailsDTO.setDoubleRoomsBooked(t.getDoubleRoomsBooked());
            tripDetailsDTO.setSingleRoomsBooked(t.getSingleRoomsBooked());
            tripDetailsDTO.setPurchaseDate(t.getPurchaseDate());
            Trip trip = t.getTrip();
            TripDTO tripDTO = tripService.tripToTripDTO(trip);
            tripDetailsDTO.setTrip(tripDTO);
            listOfRecentTripsDTO.add(tripDetailsDTO);
        }
    return listOfRecentTripsDTO;
    }
}
