package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.Airport;
import travel.agency.persistance.entities.City;

import javax.persistence.Query;
import java.util.List;

@Repository
public class AirportDAO {
    HibernateUtil hibernateUtil;

    public AirportDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertAirport(Airport airport){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(airport);

        transaction.commit();
        session.close();
    }

    public Airport findAirportByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findAirport = session.createNamedQuery("find_airport_by_name").setParameter("name", name);
        Airport airport = (Airport)findAirport.getSingleResult();

        transaction.commit();
        session.close();
        return airport;
    }

    public boolean verifyAirportByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        boolean isAirport = false;
        Query findAirport = session.createNamedQuery("find_airport_by_name").setParameter("name", name);
        int numOfAirport = findAirport.getFirstResult();
        if(numOfAirport !=0){
            isAirport = true;
        }
        transaction.commit();
        session.close();
        return isAirport;
    }

    public List<Airport> findAirportsByCity(City city){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findAirportByCity = session.createNamedQuery("find_airport_by_city").setParameter("cityName", city.getName());
        List<Airport> listOfAirportsInCity = findAirportByCity.getResultList();

        transaction.commit();
        session.close();
        return listOfAirportsInCity;
    }
}
