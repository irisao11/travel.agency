package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.TripDetails;

import javax.persistence.Query;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Repository
public class TripDetailsDAO {
   private HibernateUtil hibernateUtil;

    public TripDetailsDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void addTripDetails(TripDetails tripDetails){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(tripDetails);

        transaction.commit();
        session.close();
    }

    public List<TripDetails> findListOfRecentTrips(String userName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Date fromDate = new Date(Calendar.getInstance().getTime().getTime());
        System.out.println("fromDate"+fromDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fromDate);

        calendar.add(Calendar.MONTH, -1);
        Date toDate = new Date(calendar.getTime().getTime());
        System.out.println("toDate"+toDate);
        Query findRecentTripsBought = session.createNamedQuery("find_recent_trip_details_for_user")
                                            .setParameter("userName", userName)
                                            .setParameter("fromDate", toDate)
                                            .setParameter("toDate", fromDate);

        List<TripDetails> listOfRecentTrips = findRecentTripsBought.getResultList();

        transaction.commit();
        session.close();
        return listOfRecentTrips;
    }


}
