package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.Continent;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ContinentDAO {
    private HibernateUtil hibernateUtil;

    public ContinentDAO(){
        hibernateUtil = new HibernateUtil();
    }

    public List<Continent> findListOfContinents(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        List<Continent> continents = new ArrayList<>();
        Query findListOfContinents = session.createNamedQuery("find_list_of_continents");
        continents = findListOfContinents.getResultList();

        transaction.commit();
        session.close();

        return  continents;
    }

    public Continent findContinentByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findByName=session.createNamedQuery("find_continent_by_name").setParameter("name",name);
        Continent continentByName =(Continent)findByName.getSingleResult();

        transaction.commit();
        session.close();
        return continentByName;
    }
}
