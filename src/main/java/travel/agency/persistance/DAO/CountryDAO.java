package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.business.DTO.CountryDTO;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.Country;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CountryDAO {
    private HibernateUtil hibernateUtil;

    public CountryDAO(){
        hibernateUtil = new HibernateUtil();
    }

    public void insertCountry(Country country){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(country);
        transaction.commit();
        session.close();
    }

    public Country findCountryByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findCountryByName = session.createNamedQuery("find_country_by_name");
        findCountryByName.setParameter("name", name);
        Country country = (Country)findCountryByName.getSingleResult();
        System.out.println("country"+country);

        transaction.commit();
        session.close();
        return country;
    }

    public boolean verifyCountryByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        boolean isCountry = false;

        Query findCountryByName = session.createNamedQuery("count_country");
        findCountryByName.setParameter("name", name);
        List<Long> country = findCountryByName.getResultList();
        System.out.println("numOfCountry"+country.get(0));
        if(country.get(0) != 0){
            isCountry = true;
        }
        transaction.commit();
        session.close();
        System.out.println("isCountry"+isCountry);
        return isCountry;
    }

    public List<Country> findCountries(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        List<Country>countryList = new ArrayList<>();
        Query findCountries = session.createNamedQuery("find_list_of_countries");
        countryList = findCountries.getResultList();

        transaction.commit();
        session.close();

        return countryList;
    }



}
