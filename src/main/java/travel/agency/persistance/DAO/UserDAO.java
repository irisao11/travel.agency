package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.Role;
import travel.agency.persistance.entities.User;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository
public class UserDAO {
    private HibernateUtil hibernateUtil;

    public UserDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertUser(User user) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        if (!findUserByName(user.getUserName())) {
            user.setRole(Role.USER);
            session.persist(user);
        } else {
            System.out.println("USERNAME ALREADY EXIST");

        }
        transaction.commit();
        session.close();
    }

    public boolean findUserByName(String userName) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createNamedQuery("find_user_by_name");
        query.setParameter("userName", userName);
        try {
            User user = (User) query.getSingleResult();
            transaction.commit();
            session.close();
            return true;
        } catch (NoResultException e) {
            System.out.println("Could not find any user!");
            return false;
        }
    }

    public boolean findUserByNameNoTransaction(String userName) {
        Session session = hibernateUtil.getSession();
        Query query = session.createNamedQuery("find_user_by_name");
        query.setParameter("userName", userName);
        try {
            User user = (User) query.getSingleResult();
            return true;
        } catch (NoResultException e) {
            System.out.println("Could not find any user!");
            return false;
        }
    }

    public User findUserByNameAndPassword(String userName, String password) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createNamedQuery("find_user_by_name_and_password");
        query.setParameter("userName", userName).setParameter("password", password);
        try {
            User user = (User) query.getSingleResult();
            hibernateUtil.closeSessionAndTransaction();
            return user;
        } catch (NoResultException e) {
            System.out.println("Could not find any user!");
            return null;
        }
    }

    public Role returnUserRoleNamedQuery(String userName) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createNamedQuery("return_user_role");
        query.setParameter("userName", userName);
        Role role = (Role) query.getSingleResult();
        System.out.println(role);
        transaction.commit();
        session.close();
        return role;
    }

    public boolean verifyADMIN(String userName) {
        boolean isManager = false;
        if (returnUserRoleNamedQuery(userName).equals(Role.ADMIN)) {
            isManager = true;
        } else {
            System.out.println("NOT AN ADMIN");
        }
        System.out.println(isManager);
        return isManager;
    }

    public String findPasswordNamedQuery(String userName) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createNamedQuery("find_password");
        query.setParameter("userName", userName);
        try {
            String password = (String) query.getSingleResult();
            System.out.println(password);
            transaction.commit();
            session.close();
            return password;
        } catch (NoResultException e) {
            System.out.println("Could not find any user!");
            return null;
        }
    }

    public String findSaltNamedQuery(String userName) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createNamedQuery("find_salt");
        query.setParameter("userName", userName);
        try {
            String salt = (String) query.getSingleResult();
            System.out.println(salt);
            transaction.commit();
            session.close();
            return salt;
        } catch (NoResultException e) {
            System.out.println("Could not find this user");
            return null;
        }
    }

    public int getUserId(String userName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findUserId = session.createNamedQuery("find_id_for_user")
                .setParameter("userName", userName);

        int userId = (int)findUserId.getSingleResult();
    return userId;
    }

}
