package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.Hotel;
import travel.agency.persistance.entities.Room;


import javax.persistence.Query;
import java.sql.Date;

@Repository
public class RoomDAO {
    private HibernateUtil hibernateUtil;

    public RoomDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void addRoom(Room room){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(room);

        transaction.commit();
        session.close();
    }

   public Room findRoomsInHotel(Hotel hotel){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findRoomsInHotel = session.createNamedQuery("find_rooms_in_hotel").setParameter("hotel", hotel);
        Room room = (Room) findRoomsInHotel.getSingleResult();
        return room;
   }

   public Long countNumberOfBedsAvailableForHotelAndPeriod(String hotelName, String cityName, Date departureDate, Date returnDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query countDoubleRooms = session.createNamedQuery("count_double_by__hotel_and_period")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", cityName)
                .setParameter("fromDate", departureDate)
                .setParameter("toDate", returnDate);
        Long bedsInDoubleRooms = 2*(Long)countDoubleRooms.getSingleResult();
       System.out.println("bedsIndoubleRoom ="+bedsInDoubleRooms);
        Query countSingleRooms = session.createNamedQuery("count_single_by_hotel_and_period")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", cityName)
                .setParameter("fromDate", departureDate)
                .setParameter("toDate", returnDate);
        Long bedsInSingleRooms =(Long) countSingleRooms.getSingleResult();
       System.out.println("bedsInSingle="+bedsInSingleRooms);
        Query countExtraBeds = session.createNamedQuery("count_extra_by_hotel_and_period")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", cityName)
                .setParameter("fromDate", departureDate)
                .setParameter("toDate", returnDate);
        Long extraBeds = (Long)countExtraBeds.getSingleResult();

        Long totalNumOfBeds = bedsInDoubleRooms+bedsInSingleRooms+extraBeds;
       System.out.println("totalBeds="+totalNumOfBeds);
   return totalNumOfBeds;
   }

   public Long getPriceForDoubleRoomToHotelAndPeriod(String hotelName, String cityName, Date departureDate, Date returnDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

       Query priceForDouble = session.createNamedQuery("select_price_for_double")
               .setParameter("hotelName", hotelName)
               .setParameter("hotelCity", cityName)
               .setParameter("fromDate", departureDate)
               .setParameter("toDate", returnDate);
       Long priceForDoubleRoom = (Long)priceForDouble.getSingleResult();

       transaction.commit();
       session.close();
   return priceForDoubleRoom;
   }

    public Long getPriceForSingleRoomToHotelAndPeriod(String hotelName, String cityName, Date departureDate, Date returnDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query priceForSingle = session.createNamedQuery("select_price_for_single")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", cityName)
                .setParameter("fromDate", departureDate)
                .setParameter("toDate", returnDate);
        Long priceForSingleRoom = (Long)priceForSingle.getSingleResult();

        transaction.commit();
        session.close();
        return priceForSingleRoom;
    }

    public Long countNumberOfDoubleRoomsAvailableForHotelAndPeriod(String hotelName, String cityName, Date departureDate, Date returnDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query countDoubleRooms = session.createNamedQuery("count_double_by__hotel_and_period")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", cityName)
                .setParameter("fromDate", departureDate)
                .setParameter("toDate", returnDate);
        Long doubleRooms = (Long)countDoubleRooms.getSingleResult();
       transaction.commit();
       session.close();

       return doubleRooms;
    }

    public Long countNumberOfSingleRoomsAvailableForHotelAndPeriod(String hotelName, String cityName, Date departureDate, Date returnDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query countSingleRooms = session.createNamedQuery("count_single_by_hotel_and_period")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", cityName)
                .setParameter("fromDate", departureDate)
                .setParameter("toDate", returnDate);
        Long singleRooms = (Long)countSingleRooms.getSingleResult();
        transaction.commit();
        session.close();

        return singleRooms;
    }

    public Room findRoomByParameters(String hotelName, String hotelCity, Date fromDate, Date toDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findRoomsByParameters = session.createNamedQuery("find_rooms_by_parameters")
                .setParameter("hotelName", hotelName)
                .setParameter("hotelCity", hotelCity)
                .setParameter("fromDate", fromDate)
                .setParameter("toDate", toDate);

        Room room = (Room) findRoomsByParameters.getSingleResult();
    transaction.commit();
    session.close();
    return room;
    }

    public void updateRooms(Room room) throws Exception {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query countRoom = session.createNamedQuery("count_rooms")
                                                    .setParameter("hotelName", room.getHotel().getName())
                                                    .setParameter("hotelCity", room.getHotel().getCity().getName())
                                                    .setParameter("fromDate", room.getFromDate())
                                                    .setParameter("toDate", room.getToDate());

        Long numberOfSetOfRooms = (Long) countRoom.getSingleResult();

        if (numberOfSetOfRooms == 1) {
            Query findRoomsByParameters = session.createNamedQuery("find_rooms_by_parameters")
                    .setParameter("hotelName", room.getHotel().getName())
                    .setParameter("hotelCity", room.getHotel().getCity().getName())
                    .setParameter("fromDate", room.getFromDate())
                    .setParameter("toDate", room.getToDate());

            Room updatedRoom = (Room) findRoomsByParameters.getSingleResult();
            updatedRoom.setDoubleRoomsAvailable(room.getDoubleRoomsAvailable());
            updatedRoom.setSingleRoomsAvailable(room.getSingleRoomsAvailable());
        } else {
            throw new Exception("Wrong request");
        }

        transaction.commit();
        session.close();
    }
}
