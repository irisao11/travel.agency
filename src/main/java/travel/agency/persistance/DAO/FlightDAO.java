package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.City;
import travel.agency.persistance.entities.Flight;
import travel.agency.persistance.entities.Room;

import javax.persistence.Query;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

@Repository
public class FlightDAO {
    HibernateUtil hibernateUtil;

    public FlightDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertFlight(Flight flight){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(flight);

        transaction.commit();
        session.close();
    }

    public Flight findFlightByNumber(String flightNumber){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findFlightByNumber = session.createNamedQuery("find_flight_by_flightNumber").setParameter("flightNumber", flightNumber);
        Flight flight = (Flight)findFlightByNumber.getSingleResult();

        transaction.commit();
        session.close();
        return flight;
    }

    public boolean verifyIfFlightByNumber(String flightNumber){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        boolean isFlight = false;
        Query findFlightByNumber = session.createNamedQuery("count_flight_by_number").setParameter("flightNumber", flightNumber);

        List<Long> numOfFlight = findFlightByNumber.getResultList();

        if(numOfFlight.get(0) != 0){
            isFlight = true;
        }
        transaction.commit();
        session.close();
        System.out.println("isFlight"+isFlight);
        return isFlight;
    }

    public List<Flight> findFlightFromCityToCityAndDate(Flight flight){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findFlightFromCityToCityAndDate = session.createNamedQuery("verify_flight_from_city_to_city")
                .setParameter("departureDate", flight.getDepartureDate())
                .setParameter("cityName", flight.getAirport().getCity().getName())
                .setParameter("returnCityName", flight.getReturnAirport().getCity().getName());

        List<Flight> listOfFlightsFromCityToCityAndDate = findFlightFromCityToCityAndDate.getResultList();

        transaction.commit();
        session.close();

        return listOfFlightsFromCityToCityAndDate;
    }

    public List<Flight> findFlightFromCityToCity(String cityName, String returnCityName, Date departureDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        System.out.println("cityName"+cityName+ "returnName"+returnCityName+"depDate"+departureDate);
        Query findFlightFromCityToCity = session.createNamedQuery("verify_flight_from_city_to_city")
                .setParameter("departureDate", departureDate)
                .setParameter("cityName", cityName)
                .setParameter("returnCityName", returnCityName);

        List<Flight> listOfFlightsFromCityToCity = findFlightFromCityToCity.getResultList();
        System.out.println("list of flights"+listOfFlightsFromCityToCity);

        transaction.commit();
        session.close();

        return listOfFlightsFromCityToCity;
    }

    public List<Flight> findFlightInCity(City city){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createNamedQuery("from_city").setParameter("cityName", city.getName());
        List<Flight> flights = query.getResultList();
        transaction.commit();
        session.close();
        return flights;
    }

    public List<Flight> findFlightsFromCityToCity(City city, City returnCity){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        List<Flight> flightsFormCityToCity = new ArrayList<>();
        Query query = session.createNamedQuery("from_city_to_city")
                .setParameter("cityName", city.getName())
                .setParameter("returnCity", returnCity.getName());

        flightsFormCityToCity = query.getResultList();
        transaction.commit();
        session.close();
        return flightsFormCityToCity;
    }

    public Long findFlightByDate(Date departureDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query flightByDate= session.createNamedQuery("count_flights_by_date").setParameter("departureDate", departureDate);
        Long numOfFlightsByDate = (Long)flightByDate.getSingleResult();

        transaction.commit();
        session.close();

        return numOfFlightsByDate ;
    }

    public Long getPriceForFlight(String departureCity, Date departureDate, String returnCity){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query getPriceForFlight = session.createNamedQuery("price_for_flight")
                                .setParameter("departureCity", departureCity)
                                .setParameter("departureDate", departureDate)
                                .setParameter("returnCity", returnCity);
        Long priceForFlight = (Long)getPriceForFlight.getSingleResult();

        transaction.commit();
        session.close();
    return priceForFlight;
    }

    public Flight findFlightByParameters(Date departureDate, String departureCity, String returnCity){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findFlight = session.createNamedQuery("verify_flight_from_city_to_city")
                .setParameter("departureDate", departureDate)
                .setParameter("cityName", departureCity)
                .setParameter("returnCityName", returnCity);
        Flight flight = (Flight)findFlight.getSingleResult();
        System.out.println("updatedFlight+"+flight);
        transaction.commit();
        session.close();
    return flight;
    }

    public void updateFlight(Flight flight) throws Exception{
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query countFlight = session.createNamedQuery( "count_flight")
                .setParameter("departureDate", flight.getDepartureDate())
                .setParameter("cityName", flight.getAirport().getCity().getName())
                .setParameter("returnCityName", flight.getReturnAirport().getCity().getName());
        Long numberOfFlights =(Long) countFlight.getSingleResult();
        if (numberOfFlights == 1) {
            Query findFlight = session.createNamedQuery("verify_flight_from_city_to_city")
                    .setParameter("departureDate", flight.getDepartureDate())
                    .setParameter("cityName", flight.getAirport().getCity().getName())
                    .setParameter("returnCityName", flight.getReturnAirport().getCity().getName());
            Flight updatedFlight = (Flight)findFlight.getSingleResult();


            updatedFlight.setAvailableNumOfSeats(flight.getAvailableNumOfSeats());

        } else {
            throw new Exception("Wrong request");
        }

        transaction.commit();
        session.close();
    }
}
