package travel.agency.persistance.DAO;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;

import travel.agency.persistance.entities.Trip;

import javax.persistence.Query;
import java.sql.Date;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Repository
public class TripDAO {
    HibernateUtil hibernateUtil;

    public TripDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void addTrip(Trip trip){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(trip);

        transaction.commit();
        session.close();
    }

    public Trip findTrips(Date departureDate, String departureCity, Date returnDate, String cityName, String hotelName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findTrip = session.createNamedQuery("find_trip")
                .setParameter("departureDate",departureDate )
                .setParameter("departureCity", departureCity)
                .setParameter("returnDate",returnDate)
                .setParameter("cityName", cityName)
                .setParameter("hotelName", hotelName);


        Trip chosenTrip = (Trip)findTrip.getSingleResult();

        transaction.commit();
        session.close();
    return chosenTrip;
    }

    public List<Trip> findListOfTripsByTripDetails(Date departureDate, Date returnDate, String departureCity,String arrivingCity ){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query findTripByTripDetails = session.createNamedQuery("find_trips_by_trip_details")
                .setParameter("departureDate", departureDate)
                .setParameter("returnDate", returnDate)
                .setParameter("departureCity", departureCity)
                .setParameter("cityName", arrivingCity );
        List<Trip> listOfTripsByDetails = findTripByTripDetails.getResultList();

        transaction.commit();
        session.close();

    return listOfTripsByDetails;
    }

    public Long countTripsByDetails(Date departureDate, Date returnDate, String departureCity,String arrivingCity){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        System.out.println("depDate "+departureDate + "retDate "+returnDate+"depCity "+ departureCity + "retCity "+arrivingCity);

        Query countTripByTripDetails = session.createNamedQuery("count_trip")
                .setParameter("departureDate", departureDate)
                .setParameter("returnDate", returnDate)
                .setParameter("departureCity", departureCity)
                .setParameter("cityName", arrivingCity );
        Long numOfTripsByDetails =(Long) countTripByTripDetails.getSingleResult();
        System.out.println( "numOfTrips ="+numOfTripsByDetails );
        transaction.commit();
        session.close();

    return numOfTripsByDetails;
    }

    public int getTripId(Date departureDate, String departureCity, Date returnDate, String cityName, String hotelName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query getTripId = session.createNamedQuery("find_trip_id")
                .setParameter("departureDate",departureDate )
                .setParameter("departureCity", departureCity)
                .setParameter("returnDate",returnDate)
                .setParameter("cityName", cityName)
                .setParameter("hotelName", hotelName);

        int tripID = (int) getTripId.getSingleResult();
    return tripID;
    }

    public List<Trip> sortTripsByCountry(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query sortByCountry = session.createNamedQuery("order_by_country");
        List<Trip> sortedByCountryTrips = sortByCountry.getResultList();

        transaction.commit();
        session.close();
    return sortedByCountryTrips;
    }

    public List<Trip> sortTripsByContinent(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query sortByContinent = session.createNamedQuery("order_by_continent");
        List<Trip> sortedByContinentTrips = sortByContinent.getResultList();

        transaction.commit();
        session.close();
        return sortedByContinentTrips;
    }

    public List<Trip> findAllTrips(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findAllTrips = session.createNamedQuery("find_list_of_trips");
        List<Trip> listOfAllTrips = findAllTrips.getResultList();

        transaction.commit();
        session.close();

        return listOfAllTrips;
    }

    public List<Trip> findPromotedTrips(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query promotedTrips = session.createNamedQuery("select_by_promoted");
        List<Trip> promotedTripsList  = promotedTrips.getResultList();

        transaction.commit();
        session.close();
    return promotedTripsList;
    }

    public List<Trip> findListOfUpcomingTrips(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Date fromDate = new Date(Calendar.getInstance().getTime().getTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fromDate);
        calendar.add(Calendar.MONTH,1);
        Date toDate = new Date(calendar.getTime().getTime());
        Query upcomingTrips = session.createNamedQuery("find_upcoming_trips")
                .setParameter("fromDate",fromDate )
                .setParameter("toDate", toDate);
        List<Trip> listOfUpcomingTrips = upcomingTrips.getResultList();

        transaction.commit();
        session.close();
        return listOfUpcomingTrips;
    }

    public List<Trip> findTripsByDepartureCity(String departureCity){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findTripsByDepartureCity = session.createNamedQuery("find_trips_by_departure_city").setParameter("departureCity", departureCity);
        List<Trip> listOfTripsFromCity = findTripsByDepartureCity.getResultList();

        transaction.commit();
        session.close();
        return listOfTripsFromCity;
    }

    public List<Trip> getTripsWhereDepartureDateBetweenDates(Date fromDate, Date toDate){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query tripsWithDepartureDate = session.createNamedQuery("find_upcoming_trips")
                        .setParameter("fromDate", fromDate)
                        .setParameter("toDate", toDate);
        List<Trip> listOfTripsWithDepartureDate = tripsWithDepartureDate.getResultList();

        transaction.commit();
        session.close();
        return listOfTripsWithDepartureDate;
    }

    public List<Trip> findTripsWithNumberOfNights(long numberOfNights){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findAllTrips = session.createNamedQuery("find_list_of_trips");
        List<Trip> listOfAllTrips = findAllTrips.getResultList();
        List<Trip> tripsWithNumberOfNights = new LinkedList<>();
        for(Trip t : listOfAllTrips){
            Calendar c1 = Calendar.getInstance();
            c1.setTime(t.getReturnFlight().getDepartureDate());
            long toDate = c1.getTime().getTime();
            Calendar c2 = Calendar.getInstance();
            c2.setTime(t.getDepartureFlight().getDepartureDate());
            long fromDate = c2.getTime().getTime();
            long difInMs = toDate-fromDate;
            long msPerDay = 1000*60*60*24;
            long numOfNightsInTrip = difInMs/msPerDay;
            System.out.println("numOfNights"+numOfNightsInTrip);
            if(numOfNightsInTrip == numberOfNights){
                tripsWithNumberOfNights.add(t);
            }
        }

        return tripsWithNumberOfNights;
    }



}
