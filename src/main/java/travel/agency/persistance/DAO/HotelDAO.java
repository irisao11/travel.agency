package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.Hotel;
import travel.agency.persistance.entities.Room;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class HotelDAO {
    HibernateUtil hibernateUtil;

    public HotelDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertHotel(Hotel hotel){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(hotel);

        transaction.commit();
        session.close();
    }

    public Hotel findHotelByName(String name, String cityName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findHotelByName = session.createNamedQuery("find_hotel_by_name");
        findHotelByName.setParameter("name", name).setParameter("cityName", cityName);
        Hotel hotel = (Hotel) findHotelByName.getSingleResult();

        transaction.commit();
        session.close();
        return hotel;
    }

    public boolean verifyHotelByName(String name, String cityName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        boolean isHotel = false;
        Query findHotelByName = session.createNamedQuery("count_hotel");
        findHotelByName.setParameter("name", name).setParameter("cityName", cityName);
        List<Long> numOfHotel = findHotelByName.getResultList();
        if(numOfHotel.get(0
        )!=0){
            isHotel = true;
        }

        transaction.commit();
        session.close();
        return isHotel;
    }

    public List<Room> findListOfRoomsInHotel(Hotel hotel){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();


        List<Room> listOfRoomsInHotel = hotel.getRooms();
        transaction.commit();
        session.close();

        return listOfRoomsInHotel;
    }


    public List<Hotel> findHotel(Hotel hotel){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        List<Hotel>hotelList = new ArrayList<>();
        Query findHotels = session.createNamedQuery("find_list_of_cities");
        hotelList = findHotels.getResultList();

        transaction.commit();
        session.close();

        return hotelList;
    }

    public List<Hotel> findAllHotels(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findAllHotels = session.createNamedQuery("find_list_of_hotels");
        List<Hotel> allHotels = findAllHotels.getResultList();

        transaction.commit();
        session.close();
        return allHotels;
    }


    public void updateHotel(Hotel hotel) throws Exception{
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query countHotel = session.createNamedQuery("count_hotel");
        countHotel.setParameter("name", hotel.getName());
        Long numOfHotels = (Long)countHotel.getSingleResult();
        //System.out.println("numOfCCountriest"+numOfHotels);
        // System.out.println(cement.getType());
        if(numOfHotels ==1) {
            Query updateHotel = session.createNamedQuery("find_hotel_by_name");
            updateHotel.setParameter("name", hotel.getName()).setParameter("cityName", hotel.getCity().getName());
            Hotel updatedHotel = (Hotel) updateHotel.getSingleResult();
            updatedHotel.setCity(hotel.getCity());
            updatedHotel.setHotelDescription(hotel.getHotelDescription());
            updatedHotel.setStandard(hotel.getStandard());
        }else {
            throw new Exception("Bad Request");
        }
        transaction.commit();
        session.close();
    }

    public int deleteHotel(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query deleteHotel = session.createNamedQuery("delete_hotel_by_name");
        int deletedHotel = deleteHotel.executeUpdate();

        transaction.commit();
        session.close();
        return deletedHotel;
    }
}
