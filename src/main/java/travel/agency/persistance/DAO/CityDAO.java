package travel.agency.persistance.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import travel.agency.persistance.config.HibernateUtil;
import travel.agency.persistance.entities.City;


import javax.persistence.Query;
import java.util.List;

@Repository
public class CityDAO {
    HibernateUtil hibernateUtil;

    public CityDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertCity(City city){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(city);

        transaction.commit();
        session.close();
    }

    public List<City> findListOfCities(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findListOfCities = session.createNamedQuery("find_list_of_cites");
        List<City> cityList = findListOfCities.getResultList();

        transaction.commit();
        session.close();
        return cityList;
    }

    public City findCityByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Query findCityByName = session.createNamedQuery("find_city_by_name").setParameter("name", name);
        City city = (City)findCityByName.getSingleResult();

        transaction.commit();
        session.close();
        return city;
    }

    public boolean verifyCityByName(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        boolean isCity = false;

        Query findCityByName = session.createNamedQuery("count_city").setParameter("name", name);
        List<Long> numOfCity = findCityByName.getResultList();

        if(numOfCity.get(0)!=0){
            isCity = true;
        }
        transaction.commit();
        session.close();
        return isCity;
    }

}
