package travel.agency.persistance.entities;

public enum Role {
    ADMIN,
    USER
}
