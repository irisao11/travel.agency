package travel.agency.persistance.entities;

import javax.persistence.*;
import java.util.List;
@NamedQueries({
        @NamedQuery(name = "delete_hotel_by_name",
                query = "delete from Hotel h where h.name = :name"),
        @NamedQuery(name = "find_hotel_by_name",
                query = "select h from Hotel h where h.name = :name and h.city.name = :cityName"),
        @NamedQuery(name="find_list_of_hotels",
                query = "select h from Hotel h"),
        @NamedQuery(name = "count_hotel",
                query = "select count(*) from Hotel h where h.name = :name and h.city.name = :cityName")
})

@Entity
@Table(name = "hotels",
        uniqueConstraints = { @UniqueConstraint( columnNames = { "hotel_name", "city_id" } ) } )
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "hotel_name")
    private String name;

    @Column(name = "hotel_description")
    private String hotelDescription;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "standard")
    private int standard;

    @OneToMany( mappedBy = "hotel")
    private List<Room> rooms;

    public Hotel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHotelDescription() {
        return hotelDescription;
    }

    public void setHotelDescription(String hotelDescription) {
        this.hotelDescription = hotelDescription;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

   public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
       this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hotelDescription='" + hotelDescription + '\'' +
                ", city=" + city +
                ", standard=" + standard +

                '}';
    }
}
