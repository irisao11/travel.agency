package travel.agency.persistance.entities;

import javax.persistence.*;
import java.sql.Date;

@NamedQueries({
        @NamedQuery(name = "find_recent_trip_details_for_user",
                query = "select t from TripDetails t where t.user.userName = :userName and t.purchaseDate between :fromDate and :toDate")
})

@Entity
@Table(name = "trip_details")
public class TripDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "trip_id")
    private Trip trip;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "number", unique = true)
    private String number;

    @Column(name="double_rooms")
    private Long doubleRoomsBooked;

    @Column(name="single_rooms")
    private Long singleRoomsBooked;

    @Column(name="extra_bed")
    private Long extraBedsBooked;

    @Column(name="amount_spent")
    private Long amountSpent;

    @Column(name = "date_of_purchase")
    private Date purchaseDate;

    public TripDetails() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getDoubleRoomsBooked() {
        return doubleRoomsBooked;
    }

    public void setDoubleRoomsBooked(Long doubleRoomsBooked) {
        this.doubleRoomsBooked = doubleRoomsBooked;
    }

    public Long getSingleRoomsBooked() {
        return singleRoomsBooked;
    }

    public void setSingleRoomsBooked(Long singleRoomsBooked) {
        this.singleRoomsBooked = singleRoomsBooked;
    }

    public Long getExtraBedsBooked() {
        return extraBedsBooked;
    }

    public void setExtraBedsBooked(Long extraBedsBooked) {
        this.extraBedsBooked = extraBedsBooked;
    }

    public Long getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(Long amountSpent) {
        this.amountSpent = amountSpent;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    @Override
    public String toString() {
        return "TripDetails{" +
                "id=" + id +
                ", trip=" + trip +
                ", user=" + user +
                ", number='" + number + '\'' +
                ", doubleRoomsBooked=" + doubleRoomsBooked +
                ", singleRoomsBooked=" + singleRoomsBooked +
                ", extraBedsBooked=" + extraBedsBooked +
                ", amountSpent=" + amountSpent +
                '}';
    }
}
