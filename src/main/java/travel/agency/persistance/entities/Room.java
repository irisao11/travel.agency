package travel.agency.persistance.entities;

import javax.persistence.*;
import java.sql.Date;
@NamedQueries({
        @NamedQuery(name = "delete_room_by_hotel_name",
                query = "delete from Room r where r.hotel.name = :hotelName"),
        @NamedQuery(name = "count_double_by__hotel_and_period",
                query = "select r.doubleRoomsAvailable from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name = "count_single_by_hotel_and_period",
                query = "select r.singleRoomsAvailable from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name = "count_extra_by_hotel_and_period",
                query = "select r.extraBedsAvailable from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name = "select_price_for_double",
                query = "select r.priceForDouble from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name = "select_price_for_single",
                query = "select r.priceForSingle from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name="find_list_of_room",
                query = "select r from Room r"),
        @NamedQuery(name = "count_rooms",
                query = "select count (*) from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name = "find_rooms_by_parameters",
                query = "select r from Room r where r.hotel.name = :hotelName and r.hotel.city.name = :hotelCity and r.fromDate = :fromDate and r.toDate = :toDate"),
        @NamedQuery(name = "find_rooms_in_hotel",
                query = "select r from Room r where r.hotel = :hotel")
})


@Entity
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "from_date")
    private Date fromDate;

    @Column(name = "to_date")
    private Date toDate;

    @Column(name = "num_of_double_available")
    private Long doubleRoomsAvailable;

    @Column(name = "num_of_single_available")
    private Long singleRoomsAvailable;

    @Column(name = "num_of_extra_beds")
    private Long extraBedsAvailable;

    @Column(name = "price_for_double")
    private Long priceForDouble;

    @Column(name = "price_for_single")
    private Long priceForSingle;

    @Column(name = "price_for_extra_bed")
    private Long priceForExtra;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    public Room() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Long getDoubleRoomsAvailable() {
        return doubleRoomsAvailable;
    }

    public void setDoubleRoomsAvailable(Long doubleRoomsAvailable) {
        this.doubleRoomsAvailable = doubleRoomsAvailable;
    }

    public Long getSingleRoomsAvailable() {
        return singleRoomsAvailable;
    }

    public void setSingleRoomsAvailable(Long singleRoomsAvailable) {
        this.singleRoomsAvailable = singleRoomsAvailable;
    }

    public Long getExtraBedsAvailable() {
        return extraBedsAvailable;
    }

    public void setExtraBedsAvailable(Long extraBedsAvailable) {
        this.extraBedsAvailable = extraBedsAvailable;
    }

    public Long getPriceForDouble() {
        return priceForDouble;
    }

    public void setPriceForDouble(Long priceForDouble) {
        this.priceForDouble = priceForDouble;
    }

    public Long getPriceForSingle() {
        return priceForSingle;
    }

    public void setPriceForSingle(Long priceForSingle) {
        this.priceForSingle = priceForSingle;
    }

    public Long getPriceForExtra() {
        return priceForExtra;
    }

    public void setPriceForExtra(Long priceForExtra) {
        this.priceForExtra = priceForExtra;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", doubleRoomsAvailable=" + doubleRoomsAvailable +
                ", singleRoomsAvailable=" + singleRoomsAvailable +
                ", extraBedsAvailable=" + extraBedsAvailable +
                ", priceForDouble=" + priceForDouble +
                ", priceForSingle=" + priceForSingle +
                ", priceForExtra=" + priceForExtra +
                ", hotel=" + hotel +
                '}';
    }
}
