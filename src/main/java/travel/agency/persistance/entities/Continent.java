package travel.agency.persistance.entities;

import javax.persistence.*;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "delete_continent_by_name",
                query = "delete from Continent c where c.name = :name"),
        @NamedQuery(name = "find_continent_by_name",
                query = "select c from Continent c where c.name = :name"),
        @NamedQuery(name="find_list_of_continents",
                query = "select c from Continent c")
})

@Entity
@Table(name="continents")
public class Continent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "continent_name", unique = true)
    private String name;

    @OneToMany( mappedBy = "continent")
    private List<Country> countries;

    public Continent() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        return "Continent{" +
                "id=" + id +
                ", name='" + name + '\'' +

                '}';
    }
}
