package travel.agency.persistance.entities;

import javax.persistence.*;
import java.util.List;
@NamedQueries({
        @NamedQuery(name = "delete_city_by_name",
                query = "delete from City c where c.name = :name"),
        @NamedQuery(name = "find_city_by_name",
                query = "select c from City c where c.name = :name"),
        @NamedQuery(name="find_list_of_cites",
                query = "select c from City c"),
        @NamedQuery(name = "count_city",
                query = "select count(*) from City c where c.name = :name"),
       // @NamedQuery(name = "find_city_in_country",
             //  query = "select c from City c where c.country.name = :countryName")
})

@Entity
@Table(name = "cities",
        uniqueConstraints = { @UniqueConstraint( columnNames = { "city_name", "country_id" } ) } )
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "city_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany( mappedBy = "city")
    private List<Hotel> hotels;

    @OneToMany( mappedBy = "city")
    private List<Airport> airports;

    public City() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Hotel> getHotels() {
       return hotels;
   }

   public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    public List<Airport> getAirports() {
        return airports;
    }

    public void setAirports(List<Airport> airports) {
        this.airports = airports;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country=" + country +


                '}';
    }
}
