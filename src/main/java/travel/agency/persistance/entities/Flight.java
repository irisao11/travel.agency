package travel.agency.persistance.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "delete_flight_by_flightNumber",
                query = "delete from Flight f where f.flightNumber = :flightNumber"),
        @NamedQuery(name = "find_flight_by_flightNumber",
                query = "select f from Flight f where f.flightNumber = :flightNumber"),
        @NamedQuery(name = "verify_flight_from_city_to_city",
                query = "select f from Flight f where f.departureDate = :departureDate and f.airport.city.name = :cityName and f.returnAirport.city.name = :returnCityName"),
        @NamedQuery(name = "from_city",
                query = "select f from Flight f where f.airport.city.name = :cityName"),
        @NamedQuery(name = "from_city_to_city",
                query = "select f from Flight  f where f.airport.city.name = :cityName and f.returnAirport.city.name = :returnCity"),
        @NamedQuery(name = "count_flights_by_date",
                query = "select count(*) from Flight f where f.departureDate = :departureDate"),
        @NamedQuery(name="find_list_of_flights",
                query = "select f from Flight f"),
        @NamedQuery(name = "count_flight_by_number",
                query = "select count(*) from Flight f where f.flightNumber = :flightNumber"),
        @NamedQuery(name = "price_for_flight",
                query = "select f.flightPrice from Flight f where f.airport.city.name = :departureCity and f.departureDate = :departureDate and f.returnAirport.city.name = :returnCity"),
        @NamedQuery(name = "count_flight",
                query = "select count(*) from Flight f where f.departureDate = :departureDate and f.airport.city.name = :cityName and f.returnAirport.city.name = :returnCityName")
})

@Entity
@Table(name = "flights")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "flight_number")
    private String flightNumber;

    @Column(name = "departure_date")
    private Date departureDate;

    @Column(name = "total_num_of_seats")
    private int totalNumOfSeats;

    @Column(name = "available_seats")
    private int availableNumOfSeats;

    @Column(name = "flight_price")
    private Long flightPrice;

    @ManyToOne
    @JoinColumn(name = "airport_id")
    private Airport airport;

    @ManyToOne
    @JoinColumn(name = "arriving_airport_id")
    private Airport returnAirport;

   @OneToMany( mappedBy = "departureFlight")
   private List<Trip> trips;

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "returnFlight")
    private List<Trip>returnTrips;

    public Flight() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public int getTotalNumOfSeats() {
        return totalNumOfSeats;
    }

    public void setTotalNumOfSeats(int totalNumOfSeats) {
        this.totalNumOfSeats = totalNumOfSeats;
    }

    public int getAvailableNumOfSeats() {
        return availableNumOfSeats;
    }

    public void setAvailableNumOfSeats(int availableNumOfSeats) {
        this.availableNumOfSeats = availableNumOfSeats;
    }

    public Long getFlightPrice() {
        return flightPrice;
    }

    public void setFlightPrice(Long flightPrice) {
        this.flightPrice = flightPrice;
    }

    public Airport getAirport() {
        return airport;
    }

    public void setAirport(Airport airport) {
        this.airport = airport;
    }

    public Airport getReturnAirport() {
        return returnAirport;
    }

    public void setReturnAirport(Airport returnAirport) {
        this.returnAirport = returnAirport;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public List<Trip> getReturnTrips() {
        return returnTrips;
    }

    public void setReturnTrips(List<Trip> returnTrips) {
        this.returnTrips = returnTrips;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flightNumber='" + flightNumber + '\'' +
                ", departureDate=" + departureDate +
                ", totalNumOfSeats=" + totalNumOfSeats +
                ", availableNumOfSeats=" + availableNumOfSeats +
                ", flightPrice=" + flightPrice +
                ", airport=" + airport +
                ", returnAirport=" + returnAirport +

                '}';
    }
}
