package travel.agency.persistance.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@NamedQueries({
        @NamedQuery(name = "find_user_by_name_and_password",
                query = "select u from User u where u.userName = :userName and u.password = :password"),
        @NamedQuery(name = "update_user",
                query = "update User u set u.userName = :userName, u.password = :password, u.role = :role where u.userName = :userName"),
        @NamedQuery(name = "delete_user",
                query = "delete from User u where u.userName = :userName"),
        @NamedQuery(name = "return_user_role",
                query = "select u.role from User u where u.userName = :userName"),
        @NamedQuery(name = "find_user_by_name",
                query = "select u from User u where u.userName = :userName"),
        @NamedQuery(name = "find_password",
                query = "select u.password from User u where u.userName = :userName"),
        @NamedQuery(name = "find_salt",
                query = "select u.salt from User u where u.userName = :userName"),
        @NamedQuery(name = "find_id_for_user",
                query = "select u.id from User u where u.userName = :userName"),
        @NamedQuery(name = "find_role",
                query =  "select u.role from User u where u.userName = :userName")

})
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "user_name", unique = true)
    private String userName;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "salt")
    private String salt;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "user")
    private List<TripDetails> trips = new ArrayList<>();

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public List<TripDetails> getTrips() {
        return trips;
    }

    public void setTrips(List<TripDetails> trips) {
        this.trips = trips;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(salt, user.salt) &&
                role == user.role &&
                Objects.equals(trips, user.trips);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, userName, email, password, salt, role, trips);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", role=" + role +

                '}';
    }


}
