package travel.agency.persistance.entities;

import javax.persistence.*;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "delete_airport_by_name",
                query = "delete from Airport a where a.name = :name"),
        @NamedQuery(name = "find_airport_by_name",
                query = "select a from Airport a where a.name = :name"),
        @NamedQuery(name = "find_airport_by_city",
                query = "select a from Airport a where a.city.name = :cityName"),
        @NamedQuery(name="find_list_of_airports",
                query = "select a from Airport a"),
        @NamedQuery(name = "count_airport",
                query = "select count(*) from Airport a where a.name = :name")
})

@Entity
@Table(name = "airports",
        uniqueConstraints = { @UniqueConstraint( columnNames = { "airport_name", "city_id" } ) } )
public class Airport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "airport_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @OneToMany( mappedBy = "airport")
    private List<Flight> flights;

    @OneToMany( mappedBy = "returnAirport")
    private List<Flight> returnFlight;

    public Airport() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public List<Flight> getReturnFlight() {
        return returnFlight;
    }

    public void setReturnFlight(List<Flight> returnFlight) {
        this.returnFlight = returnFlight;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city=" + city +

                '}';
    }
}
