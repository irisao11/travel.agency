package travel.agency.persistance.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "delete_trip",
                query = "delete from Trip t where t.departureFlight = :departureFlight and t.returnFlight = :returnFlight and t.hotel = :hotel"),
        @NamedQuery(name = "find_trip",
                query = "select t from Trip t where t.departureFlight.departureDate = :departureDate " +
                        "and t.departureFlight.airport.city.name = :departureCity " +
                        "and t.returnFlight.departureDate = :returnDate " +
                        "and t.departureFlight.returnAirport.city.name= :cityName " +
                        "and t.hotel.name = :hotelName"),
        @NamedQuery(name = "find_trip_id",
                query = "select t.id from Trip t where t.departureFlight.departureDate = :departureDate " +
                        "and t.departureFlight.airport.city.name = :departureCity " +
                        "and t.returnFlight.departureDate = :returnDate " +
                        "and t.departureFlight.returnAirport.city.name= :cityName " +
                        "and t.hotel.name = :hotelName"),
        @NamedQuery(name="order_by_country",
                    query = "select t from Trip t group by t.hotel.city.country.name order by t.hotel.city.country.name asc"),
        @NamedQuery(name="order_by_continent",
                query = "select t from Trip t order by t.hotel.city.country.continent.name asc"),
        @NamedQuery(name = "select_by_promoted",
                query = "select t from Trip t where t.promoted = true"),
        @NamedQuery(name = "find_trips_by_trip_details",
                query = "select t from Trip t where t.departureFlight.departureDate = :departureDate " +
                        "and t.departureFlight.airport.city.name = :departureCity " +
                        "and t.returnFlight.departureDate = :returnDate " +
                        "and t.departureFlight.returnAirport.city.name= :cityName"),
        @NamedQuery(name="find_list_of_trips",
                query = "select t from Trip t"),
        @NamedQuery(name = "find_upcoming_trips",
                query = "select t from Trip t where t.departureFlight.departureDate between :fromDate and :toDate"),
        @NamedQuery(name = "find_trips_by_departure_city",
                query = "select t from Trip t where t.departureFlight.airport.city.name = :departureCity"),
        @NamedQuery(name = "count_trip",
                query = "select count(*) from Trip t where t.departureFlight.departureDate = :departureDate " +
                        "and t.departureFlight.airport.city.name = :departureCity " +
                        "and t.returnFlight.departureDate = :returnDate " +
                        "and t.departureFlight.returnAirport.city.name= :cityName")

})
@Entity
@Table(name = "trips")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "departure_flight_id")
    private Flight departureFlight;

    @ManyToOne
    @JoinColumn(name = "return_flight_id")
    private Flight returnFlight;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    @Column(name = "promoted")
    private boolean promoted;

    @OneToMany(mappedBy = "trip")
   private List<TripDetails> users = new ArrayList<>();

    public Trip() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Flight getDepartureFlight() {
        return departureFlight;
    }

    public void setDepartureFlight(Flight departureFlight) {
        this.departureFlight = departureFlight;
    }

    public Flight getReturnFlight() {
        return returnFlight;
    }

    public void setReturnFlight(Flight returnFlight) {
        this.returnFlight = returnFlight;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public boolean isPromoted() {
        return promoted;
    }

    public void setPromoted(boolean promoted) {
        this.promoted = promoted;
    }

    public List<TripDetails> getUsers() {
        return users;
    }

    public void setUsers(List<TripDetails> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", departureFlight=" + departureFlight +
                ", returnFlight=" + returnFlight +
                ", hotel=" + hotel +
                ", promoted=" + promoted +

                '}';
    }
}
